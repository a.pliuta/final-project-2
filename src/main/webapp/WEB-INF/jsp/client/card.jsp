<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>

<!doctype html>
<html>
<c:set var="title" value="Панель администратора" scope="page"/>
<jsp:include page="/WEB-INF/templates/_head.jsp"></jsp:include>
<body>
<jsp:include page="/WEB-INF/templates/_menu_customer.jsp"></jsp:include>
<div class="container">
    <div class="row">
    <div class="d-flex justify-content-end">

        <button type="button" class="btn btn-outline-secondary btn-sm mr-2"
                data-toggle="modal"
                data-target="#cardAddModal">
            <i class="material-icons">create</i>
            <fmt:message
                    key="card.menu.private_office.modal.title"/>
        </button>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="cardAddModal" tabindex="-1" role="dialog"
         aria-labelledby="cardAddModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="controller?action=edit_card" method="post">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">
                            <fmt:message
                                    key="card.menu.private_office.modal.title"/>
                        </h5>
                        <button type="button" class="close" data-dismiss="modal"
                                aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-10">
                                <label for="recipient-name" class="col-form-label">
                                    <fmt:message
                                            key="card.menu.private_office.modal.account"/>
                                </label>
                                <input type="number" min="0" minlength="1" max="99999" maxlength="5"
                                       class="form-control"
                                       name="account_number"
                                       id="recipient-name" required>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="cc-number">
                                    <fmt:message
                                            key="card.menu.private_office.modal.cardnumber"/>
                                    </label>
                                <input type="text" maxlength="16" class="form-control" id="cc-number" placeholder=""
                                       name="cc-number"
                                       required>
                                <div class="invalid-feedback">
                                    Credit card number is required
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-3 mb-3">
                                <label for="cc-expiration">
                                    <fmt:message
                                            key="card.menu.private_office.modal.expiration"/>
                                </label>
                                <input type="date" class="form-control" id="cc-expiration"
                                       name="cc-expiration"
                                       placeholder="" required>
                                <div class="invalid-feedback">
                                    Expiration date required
                                </div>
                            </div>

                        </div>
                        <hr class="mb-4">
                        <input type="hidden" name="card_id"
                               value="0">
                        <button class="btn btn-dark btn-lg btn-block" name="btnAddCard" type="submit">
                            <fmt:message
                                    key="card.menu.private_office.modal.title"/>
                        </button>

                    </div>

                </form>
            </div>
        </div>
    </div>

    <div class="d-flex justify-content-end">

        <button type="button" class="btn btn-outline-secondary btn-sm mr-2"
                data-toggle="modal"
                data-target="#paymentModal">
            <i class="material-icons">account_balance_wallet</i>
            <fmt:message key="card.menu.private_office.modal.title2"/>
        </button>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="paymentModal" tabindex="-1" role="dialog"
         aria-labelledby="paymentModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="controller?action=edit_card" method="post">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel3">
                            <fmt:message
                                    key="card.menu.private_office.modal.title2"/>
                        </h5>
                        <button type="button" class="close" data-dismiss="modal"
                                aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-10">
                                <label for="recipient-name" class="col-form-label">
                                    <fmt:message
                                            key="card.menu.private_office.modal.amount"/>
                                </label>
                                <input type="number" min="0" minlength="1"
                                       class="form-control"
                                       name="payAmount"
                                       id="recipient-name2" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="cc-number">
                                    <fmt:message
                                        key="card.menu.private_office.modal.cardnumber"/></label>
                                <input type="text" maxlength="16" class="form-control" id="cc-number2" placeholder=""
                                       name="cc-number"
                                       required>
                                <div class="invalid-feedback">
                                    Credit card number is required
                                </div>
                            </div>
                        </div>
                        <hr class="mb-4">
                        <input type="hidden" name="card_id"
                               value="0">
                        <button class="btn btn-dark btn-lg btn-block" name="btnPayCard" type="submit">
                            <fmt:message
                                    key="card.menu.private_office.modal.title2"/></button>

                    </div>

                </form>
            </div>
        </div>
    </div>
    </div>

    <c:if test="${not empty errorMessage}">
        <h5 style="color:#ff0000"> ${errorMessage}</h5>
    </c:if>
<%-- Картки--%>
<div class="tab-pane fade show active" id="v-pills-users" role="tabpanel"
     aria-labelledby="v-pills-users-tab">
    <div class="tab-content" id="usersTabContent">
        <%-- Список платежів --%>
        <div class="tab-pane fade show active" id="users" role="tabpanel"
             aria-labelledby="internet-tab">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">
                        <fmt:message key="card.th.number"/>
                    </th>
                    <th scope="col">
                        <fmt:message key="card.th.endDate"/>
                    </th>
                    <th scope="col">
                        <fmt:message key="card.th.accountNumber"/>
                    </th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="fullCard" items="${userCards}">
                    <tr>
                        <td>${fullCard.number}</td>
                            <td>${fullCard.endDate}</td>
                            <td>${fullCard.accountNumber}</td>
                  </tr>
        </c:forEach>
        </tbody>
    </table>
</div>

    </div>
</div>
</div>
<jsp:include page="/WEB-INF/templates/_scripts.jsp"></jsp:include>
</body>
</html>
