<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>

<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<!doctype html>
<html>
<c:set var="title" value="Страница входа" scope="page"/>
<jsp:include page="/WEB-INF/templates/_head.jsp"></jsp:include>
<body>
<%--<jsp:include page="/WEB-INF/templates/_menu.jsp"></jsp:include>--%>
<jsp:include page="/WEB-INF/templates/_menu_customer.jsp"></jsp:include>
<div class="container">
    <div class="row">
        <div class="col">
            <div class="dropdown">
                <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <fmt:message key="payment.sort"/>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="<c:out value="${'controller?action=account'  += '&'+= 'sort=' += 'ws'}"/>">
                        <fmt:message key="account.sort.ws"/> </a>
                    <a class="dropdown-item" href="<c:out value="${'controller?action=account'  += '&'+= 'sort=' += 'nus'}"/>">
                        <fmt:message key="account.sort.nus"/> </a>
                    <a class="dropdown-item" href="<c:out value="${'controller?action=account'  += '&'+= 'sort=' += 'nuf'}"/>">
                        <fmt:message key="account.sort.nuf"/> </a>
                    <a class="dropdown-item" href="<c:out value="${'controller?action=account'  += '&'+= 'sort=' += 'nms'}"/>">
                        <fmt:message key="account.sort.nms"/> </a>
                    <a class="dropdown-item" href="<c:out value="${'controller?action=account'  += '&'+= 'sort=' += 'nmf'}"/>">
                        <fmt:message key="account.sort.nmf"/> </a>
                    <a class="dropdown-item" href="<c:out value="${'controller?action=account'  += '&'+= 'sort=' += 'bs'}"/>">
                        <fmt:message key="account.sort.bs"/> </a>
                    <a class="dropdown-item" href="<c:out value="${'controller?action=account'  += '&'+= 'sort=' += 'bf'}"/>">
                        <fmt:message key="account.sort.bf"/> </a>
                </div>
            </div>
        </div>
        <div class="col">
        <div class="d-flex justify-content-end">
            <button type="button" class="btn btn-outline-secondary btn-sm mr-2"
                    data-toggle="modal"
                    data-target="#addCardeModal">
                <i class="material-icons">create</i>
                <fmt:message key="account.menu.private_office.modal.title2"/>
            </button>
        </div>
    <!-- Modal -->
    <div class="modal fade" id="addCardeModal" tabindex="-1" role="dialog"
         aria-labelledby="accountBalanceModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="controller?action=edit_account" method="post">
<%--                    <input type="hidden" name="version" value="${version}">--%>
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">
                            <fmt:message
                                    key="account.menu.private_office.modal.title2"/>
                        </h5>
                        <button type="button" class="close" data-dismiss="modal"
                                aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="account_name">
                                    <fmt:message key="account.menu.private_office.modal.name2"/>
                                </label>
                                <input type="text" class="form-control" name="account_name" id="account_name" placeholder=""
                                       required>
                                <div class="invalid-feedback">
                                    <fmt:message key="account.menu.private_office.modal.name2"/>
                                </div>
                            </div>

                        </div>
                        <hr class="mb-4">
                        <input type="hidden" name="account_id"
                               value="0">
                        <button class="btn btn-dark btn-lg btn-sm" name="btnAddNew" type="submit">
                            <fmt:message key="account.menu.private_office.modal.add2"/>
                        </button>

                    </div>

                </form>
            </div>
        </div>
    </div>
    </div>
    <div class="col">
        <div class="d-flex justify-content-end">
            <button type="button" class="btn btn-outline-secondary btn-sm mr-2"
                    data-toggle="modal"
                    data-target="#topUpModel1">
                <i class="material-icons">account_balance_wallet</i>
                <fmt:message key="account.menu.private_office.modal.title3"/>
            </button>
        </div>
        <!-- Modal -->
        <div class="modal fade"
             id="topUpModel1"
             tabindex="-1" role="dialog"
             aria-labelledby="accountBalanceModal3"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form action="controller?action=edit_account" method="post">
                        <div class="modal-header">
                            <h5 class="modal-title"
                                id="exampleModalLabel3">
                                <fmt:message
                                        key="account.menu.private_office.modal.title3"/>
                            </h5>
                            <button type="button" class="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-10">
                                    <label for="recipient-name" class="col-form-label">
                                        <fmt:message
                                                key="account.menu.private_office.modal.account3"/>
                                    </label>
                                    <input type="number" min="0" minlength="1" max="99999" maxlength="5"
                                           class="form-control"
                                           name="account_number"
                                           id="account-name" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="account_name">
                                        <fmt:message key="account.menu.private_office.modal.sum3"/>
                                    </label>
                                    <input type="number" min="0" minlength="1"
                                           class="form-control"
                                           name="account_addsum"
                                           id="recipient-name" required>
                                    <div class="invalid-feedback">
                                        <fmt:message key="account.menu.private_office.modal.sum3"/>
                                    </div>
                                </div>

                            </div>
                            <hr class="mb-4">
                            <input type="hidden" name="account_id"
                                   value="0">
                            <button class="btn btn-dark btn-lg btn-sm" name="btnAddSum" type="submit">
                                <fmt:message key="account.menu.private_office.modal.name3"/>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
    <c:if test="${not empty errorMessage}">
        <h5 style="color:#ff0000"> ${errorMessage}</h5>
    </c:if>
    <%-- Платежі--%>
    <div class="tab-pane fade show active" id="v-pills-users" role="tabpanel"
         aria-labelledby="v-pills-users-tab">
        <div class="tab-content" id="usersTabContent">
            <%-- Список платежів --%>
            <div class="tab-pane fade show active" id="users" role="tabpanel"
                 aria-labelledby="internet-tab">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col">
                            <fmt:message key="account.th.number"/>
                        </th>
                        <th scope="col">
                            <fmt:message key="account.th.name"/>
                        </th>
                        <th scope="col">
                            <fmt:message key="account.th.sum"/>
                        </th>
                        <th scope="col">
                            <fmt:message key="account.th.isactive"/>
                        </th>
                        <th scope="col">
                            <fmt:message key="account.th.deblock_query"/>
                        </th>
                        <th scope="col">
                            <fmt:message key="account.th.edit"/>
                        </th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="fullAccount" items="${userAccounts}">
                        <tr>
                            <td>${fullAccount.number}</td>
                            <td>${fullAccount.name}</td>
                            <td>${fullAccount.balance}</td>
                            <td><tags:isblocked value="${fullAccount.active}"/></td>
                            <td><tags:deblock_query value="${fullAccount.deblockingQuery}"/></td>
                            <td>
                                <div class="d-flex justify-content-end">
                                    <div>
                                        <div class="row">
                                        <form action="controller?action=edit_account"
                                              method="post">
                                            <input type="hidden" name="account_id"
                                                   value="${fullAccount.id}">
                                            <button type="submit"
                                                    class="btn btn-outline-secondary btn-sm"
                                                    name="btnLock">
                                                    ${fullAccount.active ? '<i class="material-icons">lock_open</i>' : '<i class="material-icons">lock</i>'}
                                            </button>
                                        </form>
                                        <form action="controller?action=edit_account"
                                              method="post">
                                            <input type="hidden" name="account_id"
                                                   value="${fullAccount.id}">
                                            <button type="submit"
                                                    class="btn btn-outline-secondary btn-sm"
                                                    name="btnDeBlock">
                                                    <i class="material-icons">send</i>
                                                    <fmt:message key="account.th.deblock_query"/>
                                            </button>
                                        </form>

                                    </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>

        </div>
    </div>

    <nav aria-label="Page navigation example">
        <ul class="pagination">
            <li class="page-item">

                <a class="page-link" href="<c:out value="${'controller?action=account'  += '&'+= 'page=' += ps.currentPage - 1  += '&'+= 'sort=' += ps.sortType}"/>" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                </a>
            </li>
            <li class="page-item"><a class="page-link" > ${ps.numberOfPages}</a></li>
            <li class="page-item">
                <a class="page-link" href="<c:out value="${'controller?action=account'  += '&'+= 'page=' += ps.currentPage + 1  += '&'+= 'sort=' += ps.sortType}"/>" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                </a>
            </li>
        </ul>
    </nav>
</div>
<jsp:include page="/WEB-INF/templates/_scripts.jsp"></jsp:include>
</body>
</html>