<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>

<!doctype html>
<html>
<c:set var="title" value="Панель администратора" scope="page"/>
<jsp:include page="/WEB-INF/templates/_head.jsp"></jsp:include>
<body>
<jsp:include page="/WEB-INF/templates/_menu_admin.jsp"></jsp:include>
<div class="container">
    <div class="dropdown">
        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <fmt:message key="payment.sort"/>
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="<c:out value="${'controller?action=accounts'  += '&'+= 'sort=' += 'ws'}"/>">
                <fmt:message key="account.sort.ws"/> </a>
            <a class="dropdown-item" href="<c:out value="${'controller?action=accounts'  += '&'+= 'sort=' += 'nus'}"/>">
                <fmt:message key="account.sort.nus"/> </a>
            <a class="dropdown-item" href="<c:out value="${'controller?action=accounts'  += '&'+= 'sort=' += 'nuf'}"/>">
                <fmt:message key="account.sort.nuf"/> </a>
            <a class="dropdown-item" href="<c:out value="${'controller?action=accounts'  += '&'+= 'sort=' += 'nms'}"/>">
                <fmt:message key="account.sort.nms"/> </a>
            <a class="dropdown-item" href="<c:out value="${'controller?action=accounts'  += '&'+= 'sort=' += 'nmf'}"/>">
                <fmt:message key="account.sort.nmf"/> </a>
            <a class="dropdown-item" href="<c:out value="${'controller?action=accounts'  += '&'+= 'sort=' += 'bs'}"/>">
                <fmt:message key="account.sort.bs"/> </a>
            <a class="dropdown-item" href="<c:out value="${'controller?action=accounts'  += '&'+= 'sort=' += 'bf'}"/>">
                <fmt:message key="account.sort.bf"/> </a>
        </div>
    </div>
<%-- Платежі--%>
<div class="tab-pane fade show active" id="v-pills-users" role="tabpanel"
     aria-labelledby="v-pills-users-tab">
    <div class="tab-content" id="usersTabContent">
        <%-- Список платежів --%>
        <div class="tab-pane fade show active" id="users" role="tabpanel"
             aria-labelledby="internet-tab">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">
                        <fmt:message key="account.th.number"/>
                    </th>
                    <th scope="col">
                        <fmt:message key="account.th.name"/>
                    </th>
                    <th scope="col">
                        <fmt:message key="account.th.sum"/>
                    </th>
                    <th scope="col">
                        <fmt:message key="account.th.isactive"/>
                    </th>
                    <th scope="col">
                        <fmt:message key="account.th.deblock_query"/>
                    </th>
                    <th scope="col">
                        <fmt:message key="account.th.owner"/>
                    </th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="fullAccount" items="${fullAccount}">
                    <tr>
<%--                        <td>${fullAccount.id}</td>--%>
                        <td>${fullAccount.number}</td>
                        <td>${fullAccount.name}</td>
                        <td>${fullAccount.balance}</td>
                        <td><tags:isblocked value="${fullAccount.active}"/></td>
                        <td><tags:deblock_query value="${fullAccount.deblockingQuery}"/></td>
                        <td>${fullAccount.userName}</td>
                        <td>
                            <div class="d-flex justify-content-end">
                                <div>
                                    <form action="controller?action=edit_accounts"
                                          method="post">
                                        <input type="hidden" name="account_id"
                                               value="${fullAccount.id}">
                                        <button type="submit"
                                                class="btn btn-outline-secondary btn-sm"
                                                name="btnLock">
                                                ${fullAccount.active ? '<i class="material-icons">lock_open</i>' : '<i class="material-icons">lock</i>'}
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </td>
                    </tr>
                </c:forEach>
        </tbody>
        </table>
    </div>

    </div>
</div>

    <nav aria-label="Page navigation example">
        <ul class="pagination">
            <li class="page-item">

                <a class="page-link" href="<c:out value="${'controller?action=accounts'  += '&'+= 'page=' += ps.currentPage - 1  += '&'+= 'sort=' += ps.sortType}"/>" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                </a>
            </li>
            <li class="page-item"><a class="page-link" > ${ps.numberOfPages}</a></li>
            <li class="page-item">
                <a class="page-link" href="<c:out value="${'controller?action=accounts'  += '&'+= 'page=' += ps.currentPage + 1  += '&'+= 'sort=' += ps.sortType}"/>" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                </a>
            </li>
        </ul>
    </nav>
</div>
<jsp:include page="/WEB-INF/templates/_scripts.jsp"></jsp:include>
</body>
</html>
