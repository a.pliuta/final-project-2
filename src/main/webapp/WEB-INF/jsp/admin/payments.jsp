<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>

<!doctype html>
<html>
<c:set var="title" value="Панель администратора" scope="page"/>
<jsp:include page="/WEB-INF/templates/_head.jsp"></jsp:include>
<body>
<jsp:include page="/WEB-INF/templates/_menu_admin.jsp"></jsp:include>
<div class="container">
    <div class="dropdown">
        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <fmt:message key="payment.sort"/>
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="<c:out value="${'controller?action=payments'  += '&'+= 'sort=' += 'ws'}"/>">
                <fmt:message key="payment.sort.ws"/> </a>
            <a class="dropdown-item" href="<c:out value="${'controller?action=payments'  += '&'+= 'sort=' += 'nus'}"/>">
                <fmt:message key="payment.sort.nus"/> </a>
            <a class="dropdown-item" href="<c:out value="${'controller?action=payments'  += '&'+= 'sort=' += 'nuf'}"/>">
                <fmt:message key="payment.sort.nuf"/> </a>
            <a class="dropdown-item" href="<c:out value="${'controller?action=payments'  += '&'+= 'sort=' += 'ds'}"/>">
                <fmt:message key="payment.sort.ds"/> </a>
            <a class="dropdown-item" href="<c:out value="${'controller?action=payments'  += '&'+= 'sort=' += 'df'}"/>">
                <fmt:message key="payment.sort.df"/> </a>
        </div>
    </div>
<%-- Платежі--%>
<div class="tab-pane fade show active" id="v-pills-users" role="tabpanel"
     aria-labelledby="v-pills-users-tab">
    <div class="tab-content" id="usersTabContent">
        <%-- Список платежів --%>
        <div class="tab-pane fade show active" id="users" role="tabpanel"
             aria-labelledby="internet-tab">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">
                            <fmt:message key="payment.th.number"/>
                        </th>
                        <th scope="col">
                            <fmt:message key="payment.th.sum"/>
                        </th>
                        <th scope="col">
                            <fmt:message key="payment.th.isent"/>
                        </th>
                        <th scope="col">
                            <fmt:message key="payment.th.filldate"/>
                        </th>
                        <th scope="col">
                            <fmt:message key="payment.th.sentdate"/>
                        </th>
                        <th scope="col">
                            <fmt:message key="payment.th.account"/>
                        </th>
                        <th scope="col">
                            <fmt:message key="payment.th.card"/>
                        </th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="fullPayment" items="${fullPayment}">
                        <tr>
                        <td>${fullPayment.number}</td>
                        <td>${fullPayment.sum}</td>
                        <td><tags:issent value="${fullPayment.sent}"/></td>
                        <td>${fullPayment.fillDate}</td>
                        <c:if test="${fullPayment.sent == true}">
                                <td>${fullPayment.sentDate}</td></c:if>
                        <c:if test="${fullPayment.sent == false}">
                                <td></td></c:if>
                        <td>${fullPayment.accountNumber}</td>
                        <td>${fullPayment.cardNumber}</td>

                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>

            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <li class="page-item">

                        <a class="page-link" href="<c:out value="${'controller?action=payments'  += '&'+= 'page=' += ps.currentPage - 1  += '&'+= 'sort=' += ps.sortType}"/>" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>
                    <li class="page-item"><a class="page-link" > ${ps.numberOfPages}</a></li>
                    <li class="page-item">
                        <a class="page-link" href="<c:out value="${'controller?action=payments'  += '&'+= 'page=' += ps.currentPage + 1  += '&'+= 'sort=' += ps.sortType}"/>" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                </ul>
            </nav>
    </div>
</div>
</div>
<jsp:include page="/WEB-INF/templates/_scripts.jsp"></jsp:include>
</body>
</html>
