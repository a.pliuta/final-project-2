<%@ attribute name="value" type="java.lang.Boolean" required="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<c:choose>
    <c:when test="${value}">
        <span class="badge badge-success text-uppercase"><fmt:message key="status.sent"/></span>
    </c:when>
    <c:otherwise>
        <span class="badge badge-primary text-uppercase"><fmt:message key="status.prepared"/></span>
    </c:otherwise>
</c:choose>