package com.epam.pliuta.payments.model.service.impl;

import com.epam.pliuta.payments.model.entity.User;
import com.epam.pliuta.payments.model.repository.IAccountRepo;
import com.epam.pliuta.payments.model.repository.IUserRepo;
import com.epam.pliuta.payments.model.service.IAccountService;
import com.epam.pliuta.payments.model.service.IUserService;
import com.epam.pliuta.payments.util.Validator;
import org.mindrot.jbcrypt.BCrypt;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * @author: Oleksandr Pliuta
 * @date: 01.06.2022
 */
public class UserServiceImpl implements IUserService {

    private final IUserRepo repo;
    private final IAccountService accountService;
    //private final IAccountService accountService;

    /**
     * All args constructor.
     */

    public UserServiceImpl(IUserRepo repo, IAccountService accountService) {
        this.repo = repo;
        this.accountService = accountService;

    }

    @Override
    public List<User> findAll() {
        return this.repo.getAll();
    }


    @Override
    public User findByLoginFullInfo(String login) {
        return null;
    }

    @Override
    public List<User> findAllFullInfo() {
        List<User> users = findAll();
        List<User> fullUsers = new ArrayList<>();
        for (User user : users) {
            fullUsers.add(userToFullUser(user));
        }
        return fullUsers;
    }

    @Override
    public User findByIDFullInfo(long id) {
        User user = find(id);
        return userToFullUser(user);
    }

    @Override
    public User find(long id) {
        return this.repo.getById(id);
    }

    @Override
    public void save(User user) {
        this.repo.create(user);
    }

    @Override
    public void update(User user) {
        this.repo.update(user);
    }

    @Override
    public void remove(int id) {
        this.repo.delete(id);
    }

    @Override
    public User findByLogin(String login) {
        return this.repo.getByLogin(login);
    }

    @Override
    public void saveLinksUsersHasTariffs(User user, String[] tariffsId) {

    }

    @Override
    public void removeLinksUsersHasTariffs(User user) {

    }

    @Override
    public void updateFullUserToSession(HttpServletRequest request, HttpSession session, User fullUser) {

    }

    private User userToFullUser(User user) {
        user.setAccounts(accountService.findAllByUserID(user.getId()));
        return user;

    }


    public boolean validateAndFillUser(User user, HttpServletRequest request){
        String errorMessage;
        String tempString = request.getParameter("login").trim();
        errorMessage = Validator.validateLogin(tempString,45);
        if (errorMessage != null) {
            request.setAttribute("errorMessage", errorMessage);
            return false;
        }
        user.setLogin(tempString);

        tempString = request.getParameter("password").trim();
        errorMessage = Validator.validateSentences(tempString, "password", 45);
        if (errorMessage != null) {
            request.setAttribute("errorMessage", errorMessage);
            return false;
        }
        user.setPassword(BCrypt.hashpw(tempString, BCrypt.gensalt()));


        tempString = request.getParameter("firstName").trim();
        errorMessage = Validator.validateNames(tempString, "firstName", 45);
        if (errorMessage != null) {
            request.setAttribute("errorMessage", errorMessage);
            return false;
        }
        user.setFirstName(tempString);

        tempString = request.getParameter("lastName").trim();
        errorMessage = Validator.validateNames(tempString, "lastName", 45);
        if (errorMessage != null) {
            request.setAttribute("errorMessage", errorMessage);
            return false;
        }
        user.setLastName(tempString);

        tempString = request.getParameter("email").trim();
        errorMessage = Validator.validateEMail(tempString);
        if (errorMessage != null) {
            request.setAttribute("errorMessage", errorMessage);
            return false;
        }
        user.setEmail(tempString);

        tempString = request.getParameter("phone").trim();
        errorMessage = Validator.validatePhone(tempString);
        if (errorMessage != null) {
            request.setAttribute("errorMessage", errorMessage);
            return false;
        }
        user.setPhone(tempString);
        return true;
    }
}
