package com.epam.pliuta.payments.model.repository;

import com.epam.pliuta.payments.model.entity.Account;
import com.epam.pliuta.payments.model.entity.Payment;
import com.epam.pliuta.payments.model.entity.User;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * * Account repository interface.
 *
 * @author: Oleksandr Pliuta
 * @date: 31.05.2022
 */
public interface IAccountRepo extends IEntityRepo<Account> {

    long newNumberAccount();

    long getNextIdValue();

    List<Account> getByUserID(long userId);

    long  getSize();

    List<Account> getAllLimitSort(long from, long size, String sortType);

    List<Account> getByUserIDLimitSort(long user_id, long from, long size, String sortType);

    long  getSizeByUserID(long userID);
    Connection updateTransactionStart(Account account);
}