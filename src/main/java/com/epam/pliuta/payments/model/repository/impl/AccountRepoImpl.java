package com.epam.pliuta.payments.model.repository.impl;

import com.epam.pliuta.payments.model.builder.AccountQueryBuilder;
import com.epam.pliuta.payments.model.builder.QueryBuilder;
import com.epam.pliuta.payments.model.connectionpool.DBManager;
import com.epam.pliuta.payments.model.entity.Account;
import com.epam.pliuta.payments.model.entity.Payment;
import com.epam.pliuta.payments.model.repository.IAccountRepo;
import com.epam.pliuta.payments.util.PaginationSort;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * @author: Oleksandr Pliuta
 * @date: 31.05.2022
 */
public class AccountRepoImpl implements IAccountRepo {
    private static final String CREATE = "INSERT INTO accounts (id, number, name, amount, is_active, deblock_query, users_id) "
            + "VALUES (?, ?, ?, ?, ?, ?, ?)";
    private static final String GET_ALL = "SELECT * FROM accounts";
    private static final String GET_ALL_LIMIT = "SELECT * FROM accounts LIMIT ? , ?";
    private static final String GET_ALL_LIMIT_SORT_NUMBER_ASC = "SELECT * FROM accounts ORDER BY number ASC LIMIT ? , ?";
    private static final String GET_ALL_LIMIT_SORT_NUMBER_DESC = "SELECT * FROM accounts ORDER BY number DESC LIMIT ? , ?";
    private static final String GET_ALL_LIMIT_SORT_NAME_ASC = "SELECT * FROM accounts ORDER BY name ASC LIMIT ? , ?";
    private static final String GET_ALL_LIMIT_SORT_NAME_DESC = "SELECT * FROM accounts ORDER BY name DESC LIMIT ? , ?";
    private static final String GET_ALL_LIMIT_SORT_AMOUNT_ASC = "SELECT * FROM accounts ORDER BY amount ASC LIMIT ? , ?";
    private static final String GET_ALL_LIMIT_SORT_AMOUNT_DESC = "SELECT * FROM accounts ORDER BY amount DESC LIMIT ? , ?";

    private static final String GET_ALL_LIMIT_USERID = "SELECT * FROM accounts WHERE users_id = ? LIMIT ? , ?";
    private static final String GET_ALL_LIMIT_SORT_NUMBER_ASC_USERID = "SELECT * FROM accounts WHERE users_id = ? ORDER BY number ASC LIMIT ? , ?";
    private static final String GET_ALL_LIMIT_SORT_NUMBER_DESC_USERID = "SELECT * FROM accounts WHERE users_id = ? ORDER BY number DESC LIMIT ? , ?";
    private static final String GET_ALL_LIMIT_SORT_NAME_ASC_USERID = "SELECT * FROM accounts WHERE users_id = ? ORDER BY name ASC LIMIT ? , ?";
    private static final String GET_ALL_LIMIT_SORT_NAME_DESC_USERID = "SELECT * FROM accounts WHERE users_id = ? ORDER BY name DESC LIMIT ? , ?";
    private static final String GET_ALL_LIMIT_SORT_AMOUNT_ASC_USERID = "SELECT * FROM accounts WHERE users_id = ? ORDER BY amount ASC LIMIT ? , ?";
    private static final String GET_ALL_LIMIT_SORT_AMOUNT_DESC_USERID = "SELECT * FROM accounts WHERE users_id = ? ORDER BY amount DESC LIMIT ? , ?";


    private static final String GET_BY_ID = "SELECT * FROM accounts WHERE id = ?";
    //private static final String GET_BY_ID = "SELECT id, number, amount, is_active, deblock_query FROM accounts WHERE id = ?";
    //private static final String GET_BY_USER_ID = "SELECT id, number, amount, is_active, deblock_query FROM accounts WHERE users_id = ?";
    private static final String GET_BY_USER_ID = "SELECT * FROM accounts WHERE users_id = ?";
    private static final String UPDATE = "UPDATE accounts SET name = ?, amount = ?, is_active = ?, deblock_query = ? WHERE id = ?";
    private static final String DELETE = "DELETE FROM accounts WHERE id = ?";
    private static final String GET_NEXT_AUTO_INCREMENT = "SELECT MAX(id)+1 FROM accounts";
    private static final String GET_MAX_ID = "SELECT MAX(id) FROM accounts";
    private static final String GET_SIZE = "SELECT count(*) FROM accounts";

    private static final String GET_SIZE_BY_ID = "SELECT count(*) FROM accounts WHERE users_id = ?";

    private DBManager instance = DBManager.getInstance();
    private QueryBuilder queryBuilder = new AccountQueryBuilder();

    @Override
    public List<Account> getAll() {
        return this.queryBuilder.executeAndReturnList(instance, GET_ALL);
    }

    public List<Account> getAllLimitSort(long from, long size, String sortType) {

        switch(sortType) {
            case PaginationSort.NUMBER_STRAIGHT:
                return this.queryBuilder.executeAndReturnList(instance, GET_ALL_LIMIT_SORT_NUMBER_ASC, from, size);
            case PaginationSort.NUMBER_FORWARD:
                return this.queryBuilder.executeAndReturnList(instance, GET_ALL_LIMIT_SORT_NUMBER_DESC, from, size);
            case PaginationSort.NAME_STRAIGHT:
                return this.queryBuilder.executeAndReturnList(instance, GET_ALL_LIMIT_SORT_NAME_ASC, from, size);
            case PaginationSort.NAME_FORWARD:
                return this.queryBuilder.executeAndReturnList(instance, GET_ALL_LIMIT_SORT_NAME_DESC, from, size);
            case PaginationSort.BALANCE_STRAIGHT:
                return this.queryBuilder.executeAndReturnList(instance, GET_ALL_LIMIT_SORT_AMOUNT_ASC, from, size);
            case PaginationSort.BALANCE_FORWARD:
                return this.queryBuilder.executeAndReturnList(instance, GET_ALL_LIMIT_SORT_AMOUNT_DESC, from, size);
            case PaginationSort.WITHOUT_SORT:
            default:
                return this.queryBuilder.executeAndReturnList(instance, GET_ALL_LIMIT, from, size);
        }
    }

    @Override
    public List<Account> getByUserIDLimitSort(long user_id, long from, long size, String sortType) {

        switch(sortType) {
            case PaginationSort.NUMBER_STRAIGHT:
                return this.queryBuilder.executeAndReturnList(instance, GET_ALL_LIMIT_SORT_NUMBER_ASC_USERID, user_id, from, size);
            case PaginationSort.NUMBER_FORWARD:
                return this.queryBuilder.executeAndReturnList(instance, GET_ALL_LIMIT_SORT_NUMBER_DESC_USERID, user_id, from, size);
            case PaginationSort.NAME_STRAIGHT:
                return this.queryBuilder.executeAndReturnList(instance, GET_ALL_LIMIT_SORT_NAME_ASC_USERID, user_id, from, size);
            case PaginationSort.NAME_FORWARD:
                return this.queryBuilder.executeAndReturnList(instance, GET_ALL_LIMIT_SORT_NAME_DESC_USERID, user_id, from, size);
            case PaginationSort.BALANCE_STRAIGHT:
                return this.queryBuilder.executeAndReturnList(instance, GET_ALL_LIMIT_SORT_AMOUNT_ASC_USERID, user_id, from, size);
            case PaginationSort.BALANCE_FORWARD:
                return this.queryBuilder.executeAndReturnList(instance, GET_ALL_LIMIT_SORT_AMOUNT_DESC_USERID, user_id, from, size);
            case PaginationSort.WITHOUT_SORT:
            default:
                return this.queryBuilder.executeAndReturnList(instance, GET_ALL_LIMIT_USERID, user_id, from, size);
        }
    }

    @Override
    public Account getById(long id) {
        return (Account) this.queryBuilder.executeAndReturn(instance, GET_BY_ID, id);
    }
    @Override
    public List<Account> getByUserID(long userId) {
        return this.queryBuilder.executeAndReturnList(instance, GET_BY_USER_ID, userId);
    }

    @Override
    public void create(Account account) {
        long id = queryBuilder.getNextAutoIncrement(instance, GET_NEXT_AUTO_INCREMENT);
        this.queryBuilder.execute(instance, CREATE, id, account.getNumber(), account.getName(), account.getBalance(),
                account.isActive(),account.isDeblockingQuery(), account.getUserID());
    }

    @Override
    public void update(Account account) {
        this.queryBuilder.execute(instance, UPDATE, account.getName(), account.getBalance(),account.isActive(),
                account.isDeblockingQuery(), account.getId());
    }

    @Override
    public void delete(long id) {
        this.queryBuilder.execute(instance, DELETE, id);
    }

    @Override
    public long newNumberAccount() {
        long accountNumber = 0;
        long id = queryBuilder.getNextAutoIncrement(instance, GET_MAX_ID);
        Account account = getById(id);

        if (account != null) {
            accountNumber = 1 + account.getNumber();
        }

        return accountNumber;
    }

    @Override
    public long getNextIdValue() {
        return queryBuilder.getNextAutoIncrement(instance, GET_NEXT_AUTO_INCREMENT);
    }

    @Override
    public long  getSize(){ return queryBuilder.executeGetSize(instance, GET_SIZE);}

    @Override
    public long  getSizeByUserID(long userID){ return queryBuilder.executeGetSizeById(instance, GET_SIZE_BY_ID, userID);}

    public Connection updateTransactionStart(Account account){
        return this.queryBuilder.executeTransactionStart(instance, UPDATE, account.getName(), account.getBalance(),account.isActive(),
                account.isDeblockingQuery(), account.getId());
    }

}
