package com.epam.pliuta.payments.model.service.impl;

import com.epam.pliuta.payments.model.entity.Account;
import com.epam.pliuta.payments.model.entity.Payment;
import com.epam.pliuta.payments.model.entity.User;
import com.epam.pliuta.payments.model.repository.IAccountRepo;
import com.epam.pliuta.payments.model.service.IAccountService;
import com.epam.pliuta.payments.model.service.ICardService;
import com.epam.pliuta.payments.model.service.IPaymentService;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author: Oleksandr Pliuta
 * @date: 01.06.2022
 */
public class AccountServiceImpl implements IAccountService {

    private final IAccountRepo repo;
    private final ICardService cardService;
    private final IPaymentService paymentService;

    /**
     * All args constructor.
     */

    public AccountServiceImpl(IAccountRepo repo, ICardService cardService, IPaymentService paymentService) {
        this.repo = repo;
        this.cardService = cardService;
        this.paymentService = paymentService;
    }


    @Override
    public List<Account> findAll() {
        return this.repo.getAll();
    }

    @Override
    public List<Account> findAllLimitSort(long from, long size, String sortType){
        return this.repo.getAllLimitSort(from, size, sortType);
    }

    @Override
    public List<Account> findAllByUserIDLimitSort(long user_id, long from, long size, String sortType){
        return this.repo.getByUserIDLimitSort(user_id, from, size, sortType);
    }

    @Override
    public Account findByNumberFullInfo(long number) {
        return null;
    }

    @Override
    public List<Account> findAllFullInfo() {
        List<Account> accounts = findAll();
        List<Account> fullAccounts = new ArrayList<>();
        for (Account account : accounts) {
            fullAccounts.add(accountToFullAccount(account));
        }
        return fullAccounts;
    }

    @Override
    public Account findByIDFullInfo(long id) {
        Account account = find(id);
        return accountToFullAccount(account);
    }
//
//    @Override
//    public User findByLoginFullInfo(String login) {
//        return userToFullUser(findByLogin(login));
//    }

    @Override
    public Account find(long id) {
        return this.repo.getById(id);
    }

    @Override
    public void save(Account account) {
        this.repo.create(account);
    }

    @Override
    public void update(Account account) {
        this.repo.update(account);
    }

    @Override
    public void remove(int id) {
        this.repo.delete(id);
    }

    @Override
    public List<Account> findByUserID(long userID) {
        return this.repo.getByUserID(userID);
    }

    @Override
    public List<Account> findAllByUserID(long userID) {
        List<Account> accounts = findByUserID(userID);
        List<Account> fullAccounts = new ArrayList<>();
        for (Account account : accounts) {
            fullAccounts.add(accountToFullAccount(account));
        }
        return fullAccounts;
    }




    private Account accountToFullAccount(Account account) {
        account.setCards(cardService.findByAccountID(account.getId()));
        account.setPayments(paymentService.findByAccountID(account.getId()));
        return account;

    }
    @Override
    public long findSize() {
        return this.repo.getSize();
    }
    @Override
    public long findSizeByUserID(long userID) {
        return this.repo.getSizeByUserID(userID);
    }
    @Override
    public long findNextNumber() {return this.repo.newNumberAccount();}
    @Override
    public Connection updateTransactionStart(Account account){
        return this.repo.updateTransactionStart(account);
    }

}
