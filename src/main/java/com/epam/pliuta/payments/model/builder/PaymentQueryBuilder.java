package com.epam.pliuta.payments.model.builder;

import com.epam.pliuta.payments.model.entity.Payment;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author: Oleksandr Pliuta
 * @date: 02.06.2022
 */
public class PaymentQueryBuilder extends QueryBuilder<Payment> {
    @Override
    public List<Payment> getListOfResult(ResultSet rs) throws SQLException {
        List<Payment> payments = new ArrayList<>();
        while (rs.next()) {
            Payment payment = new Payment();
            payment.setId(rs.getLong("id"));
            payment.setNumber(rs.getLong("number"));
            payment.setSum(rs.getBigDecimal("sum"));
            payment.setSent(rs.getBoolean("is_sent"));
            payment.setFillDate(rs.getTimestamp("fill_date").toLocalDateTime());
            payment.setSentDate(rs.getTimestamp("sent_date").toLocalDateTime());
            payment.setAccountID(rs.getLong("accounts_id"));
            payment.setCardID(rs.getLong("cards_id"));
            payment.setUserID(rs.getLong("users_id"));
            payments.add(payment);
        }
        return payments;
    }

    @Override
    public Payment getResult(ResultSet rs) throws SQLException {
        Payment payment = new Payment();
        while (rs.next()) {
            payment.setId(rs.getLong("id"));
            payment.setNumber(rs.getLong("number"));
            payment.setSum(rs.getBigDecimal("sum"));
            payment.setSent(rs.getBoolean("is_sent"));
            payment.setFillDate(rs.getTimestamp("fill_date").toLocalDateTime());
            payment.setSentDate(rs.getTimestamp("sent_date").toLocalDateTime());
            payment.setAccountID(rs.getLong("accounts_id"));
            payment.setCardID(rs.getLong("cards_id"));
            payment.setUserID(rs.getLong("users_id"));
        }
        return payment;
    }
}
