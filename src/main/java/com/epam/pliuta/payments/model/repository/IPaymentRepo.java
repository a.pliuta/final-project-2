package com.epam.pliuta.payments.model.repository;

import com.epam.pliuta.payments.model.entity.Card;
import com.epam.pliuta.payments.model.entity.Payment;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Payment repository interface.
 *
 * @author: Oleksandr Pliuta
 * @date: 31.05.2022
 */
public interface IPaymentRepo extends IEntityRepo<Payment> {

    long newNumberPayment();

    long getNextIdValue();

    List<Payment> getByAccountID(long accountId);

    long  getSize();

    List<Payment> getAllLimitSort(long from, long size, String sortType);

    List<Payment> getByUserIDLimitSort(long user_id, long from, long size, String sortType);

    long  getSizeByUserID(long userID);

    void createTransactionEnd(Payment payment,  Connection conn);
    void updateTransactionEnd(Payment payment,  Connection conn);
}