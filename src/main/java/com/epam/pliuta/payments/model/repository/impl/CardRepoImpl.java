package com.epam.pliuta.payments.model.repository.impl;

import com.epam.pliuta.payments.model.builder.CardQueryBuilder;
import com.epam.pliuta.payments.model.builder.QueryBuilder;
import com.epam.pliuta.payments.model.connectionpool.DBManager;
import com.epam.pliuta.payments.model.entity.Card;
import com.epam.pliuta.payments.model.repository.ICardRepo;

import java.util.HashMap;
import java.util.List;

/**
 * @author: Oleksandr Pliuta
 * @date: 02.06.2022
 */
public class CardRepoImpl implements ICardRepo {
    private static final String CREATE = "INSERT INTO cards (id, card_number, end_date, accounts_id, users_id) "
            + "VALUES (?, ?, ?, ?, ?)";
    private static final String GET_ALL = "SELECT * FROM cards";
    private static final String GET_BY_ID = "SELECT * FROM cards WHERE id = ?";
    private static final String GET_BY_ACCOUNT_ID = "SELECT * FROM cards WHERE accounts_id = ?";
    private static final String GET_BY_USER_ID = "SELECT * FROM cards WHERE users_id = ?";
    private static final String UPDATE = "UPDATE cards SET card_number = ?, end_date = ?, accounts_id = ?, users_id = ?  WHERE id = ?";
    private static final String DELETE = "DELETE FROM cards WHERE id = ?";
    private static final String GET_NEXT_AUTO_INCREMENT = "SELECT MAX(id)+1 FROM cards";
    private static final String GET_MAX_ID = "SELECT MAX(id) FROM cards";

    private DBManager instance = DBManager.getInstance();
    private QueryBuilder queryBuilder = new CardQueryBuilder();

    @Override
    public List<Card> getAll() {
        return this.queryBuilder.executeAndReturnList(instance, GET_ALL);
    }

    @Override
    public Card getById(long id) {
        return (Card) this.queryBuilder.executeAndReturn(instance, GET_BY_ID, id);
    }
    @Override
    public List<Card> getByAccountID(long accountId) {
        return this.queryBuilder.executeAndReturnList(instance, GET_BY_ACCOUNT_ID, accountId);
    }

    @Override
    public List<Card> getByUserID(long userId) {
        return this.queryBuilder.executeAndReturnList(instance, GET_BY_USER_ID, userId);
    }

    @Override
    public void create(Card card) {
        long id = queryBuilder.getNextAutoIncrement(instance, GET_NEXT_AUTO_INCREMENT);
        this.queryBuilder.execute(instance, CREATE, id, card.getNumber(), card.getEndDate(), card.getAccountID(), card.getUserID());
    }

    @Override
    public void update(Card card) {
        this.queryBuilder.execute(instance, UPDATE, card.getNumber(), card.getEndDate(), card.getAccountID(), card.getUserID(), card.getId());
    }

    @Override
    public void delete(long id) {
        this.queryBuilder.execute(instance, DELETE, id);
    }

    @Override
    public long getNextIdValue() {
        return queryBuilder.getNextAutoIncrement(instance, GET_NEXT_AUTO_INCREMENT);
    }

}

