package com.epam.pliuta.payments.model.entity;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

/**
 * @author: Oleksandr Pliuta
 * @date: 31.05.2022
 */
public class Account extends Entity {
    private long number;
    private String name;
    private BigDecimal balance;
    private boolean active;
    private boolean deblockingQuery;
    private long userID;

    private String userName;

    public long getUserID() {
        return userID;
    }

    public void setUserID(long userID) {
        this.userID = userID;
    }

    private List<Card> cards;

    private List<Payment> payments;

    public List<Payment> getPayments() {
        return payments;
    }

    public void setPayments(List<Payment> payments) {
        this.payments = payments;
    }

    public Account() {
    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public boolean isDeblockingQuery() {
        return deblockingQuery;
    }

    public void setDeblockingQuery(boolean deblockingQuery) {
        this.deblockingQuery = deblockingQuery;
    }

    public List<Card> getCards() {
        return cards;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }

    public String getUserName() {return userName;}

    public void setUserName(String userName) {this.userName = userName;}


    public boolean isActive() {return active;}

    public void setActive(boolean active) {this.active = active;}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "\n      Account{" +
                "number=" + number +
                ", name='" + name + '\'' +
                ", balance=" + balance +
                ", active=" + active +
                ", deblockingQuery=" + deblockingQuery +
                ", userID=" + userID +
                ", userName='" + userName + '\'' +
                ", cards=" + cards +
                ", payments=" + payments +
                '}';
    }
}
