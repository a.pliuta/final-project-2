package com.epam.pliuta.payments.model.service.impl;

import com.epam.pliuta.payments.model.entity.Account;
import com.epam.pliuta.payments.model.entity.Card;
import com.epam.pliuta.payments.model.repository.ICardRepo;
import com.epam.pliuta.payments.model.service.ICardService;

import java.util.HashMap;
import java.util.List;

/**
 * @author: Oleksandr Pliuta
 * @date: 01.06.2022
 */
public class CardServiceImpl implements ICardService {

    private final ICardRepo repo;

    public CardServiceImpl(ICardRepo repo) {
        this.repo = repo;
    }

    @Override
    public List<Card> findAll() {
        return this.repo.getAll();
    }

    @Override
    public Card find(long id) {
        return this.repo.getById(id);
    }

    @Override
    public void save(Card card) {
        this.repo.create(card);
    }

    @Override
    public void update(Card card) {this.repo.update(card);}

    @Override
    public void remove(int id) {
        this.repo.delete(id);
    }

    @Override
    public List<Card> findByAccountID(long accountID) {
        return this.repo.getByAccountID(accountID);
    }
    @Override
    public List<Card> findByUserID(long accountID) {return this.repo.getByUserID(accountID);}

}
