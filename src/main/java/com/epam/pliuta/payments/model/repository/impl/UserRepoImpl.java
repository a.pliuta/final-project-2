package com.epam.pliuta.payments.model.repository.impl;

import com.epam.pliuta.payments.model.builder.QueryBuilder;
import com.epam.pliuta.payments.model.builder.UserQueryBuilder;
import com.epam.pliuta.payments.model.connectionpool.DBManager;
import com.epam.pliuta.payments.model.entity.User;
import com.epam.pliuta.payments.model.repository.IUserRepo;

import java.util.List;

/**
 * @author: Oleksandr Pliuta
 * @date: 01.06.2022
 */
public class UserRepoImpl implements IUserRepo {
    private static final String GET_ALL = "SELECT * FROM users";
    private static final String GET_BY_ID =
            "SELECT id, first_name, last_name, is_active, login, password,  email,  phone,  roles_id"
                    + " FROM users WHERE id = ?";
    private static final String GET_BY_LOGIN =
            "SELECT id, first_name, last_name, is_active, login, password,  email,  phone, roles_id "
                    + " FROM users WHERE login = ?";
    private static final String CREATE =
            "INSERT INTO users (id, first_name, last_name, is_active, login, password, email, phone, roles_id) "
                    + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String UPDATE =
            "UPDATE users SET first_name = ?, last_name = ?, is_active = ?, login = ?, password = ?, email = ?, "
                    + " phone = ?, roles_id = ? WHERE id = ?";
    private static final String DELETE = "DELETE FROM clients WHERE id = ?";

    private static final String GET_NEXT_AUTO_INCREMENT = "SELECT MAX(id)+1 from users";

    private DBManager instance = DBManager.getInstance();
    private QueryBuilder queryBuilder = new UserQueryBuilder();

    @Override
    public List<User> getAll() {
        return queryBuilder.executeAndReturnList(instance, GET_ALL);
    }

    @Override
    public User getById(long id) {
        return (User) queryBuilder.executeAndReturn(instance, GET_BY_ID, id);
    }

    @Override
    public void create(User user) {
        long id = queryBuilder.getNextAutoIncrement(instance, GET_NEXT_AUTO_INCREMENT);
        queryBuilder.execute(instance, CREATE, id, user.getFirstName(), user.getLastName(), user.isActive(),
                user.getLogin(), user.getPassword(), user.getEmail(), user.getPhone(), user.getRoleId());
    }

    @Override
    public void update(User user) {
        queryBuilder.execute(instance, UPDATE, user.getFirstName(), user.getLastName(), user.isActive(),
                user.getLogin(), user.getPassword(), user.getEmail(), user.getPhone(), user.getRoleId(),
                 user.getId());
    }

    @Override
    public void delete(long id) {
        queryBuilder.execute(instance, DELETE, id);
    }

    @Override
    public User getByLogin(String login) {
        return (User) queryBuilder.executeAndReturn(instance, GET_BY_LOGIN, login);
    }

}