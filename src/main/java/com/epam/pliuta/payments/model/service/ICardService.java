package com.epam.pliuta.payments.model.service;

import com.epam.pliuta.payments.model.entity.Card;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;

/**
 * @author: Oleksandr Pliuta
 * @date: 01.06.2022
 */
public interface ICardService {
    List<Card> findAll();

    Card find(long id);

    void save(Card card);

    void update(Card card);

    void remove(int id);

    public List<Card> findByAccountID(long accountID);
    public List<Card> findByUserID(long accountID);

}
