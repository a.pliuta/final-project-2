package com.epam.pliuta.payments.model.entity;

import java.io.Serializable;

/**
 * @author: Oleksandr Pliuta
 * @date: 31.05.2022
 */
public abstract class Entity implements Serializable {
    private static final long serialVersionUID = 1L;
    private long id;

    public Entity() {
    }

    public Entity(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}

