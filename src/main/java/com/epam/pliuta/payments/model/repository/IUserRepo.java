package com.epam.pliuta.payments.model.repository;

import com.epam.pliuta.payments.model.entity.User;

import java.util.List;

/**
 *  * User repository interface.
 *
 * @author: Oleksandr Pliuta
 * @date: 31.05.2022
 */
public interface IUserRepo extends IEntityRepo<User> {

    User getByLogin(String login);

//    List<Tariff> getTariffs(User user);
//
//    void addLinksUsersHasTariffs(User user, String[] tariffsId);
//
//    void deleteLinksUsersHasTariffs(User user);
}