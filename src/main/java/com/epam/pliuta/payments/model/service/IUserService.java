package com.epam.pliuta.payments.model.service;

import com.epam.pliuta.payments.model.entity.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author: Oleksandr Pliuta
 * @date: 01.06.2022
 */
public interface IUserService {

    List<User> findAll();

    List<User> findAllFullInfo();

    User findByLoginFullInfo(String login);

    User findByIDFullInfo(long id);

    User find(long id);

    void save(User user);

    void update(User user);

    void remove(int id);

    User findByLogin(String login);

//    List<Tariff> findUserTariffs(User user);

    void saveLinksUsersHasTariffs(User user, String[] tariffsId);

    void removeLinksUsersHasTariffs(User user);

    void updateFullUserToSession(HttpServletRequest request, HttpSession session, User fullUser);

    boolean validateAndFillUser(User user, HttpServletRequest request);
}

