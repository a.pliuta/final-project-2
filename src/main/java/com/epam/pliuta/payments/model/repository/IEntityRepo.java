package com.epam.pliuta.payments.model.repository;

import com.epam.pliuta.payments.model.entity.Entity;

import java.util.List;

/**
 * * Main CRUD repository interface.
 *
 * @author: Oleksandr Pliuta
 * @date: 31.05.2022
 */
public interface IEntityRepo<T extends Entity> {
    List<T> getAll();

    T getById(long id);

    void create(T t);

    void update(T t);

    void delete(long id);

}
