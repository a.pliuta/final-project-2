package com.epam.pliuta.payments.model.builder;

import com.epam.pliuta.payments.model.entity.Account;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *  * Account entity query builder.
 *
 * @author: Oleksandr Pliuta
 * @date: 31.05.2022
 */
public class AccountQueryBuilder extends QueryBuilder<Account> {
    @Override
    public List<Account> getListOfResult(ResultSet rs) throws SQLException {
        List<Account> accounts = new ArrayList<>();
        while (rs.next()) {
            Account account = new Account();
            account.setId(rs.getLong("id"));
            account.setNumber(rs.getLong("number"));
            account.setName(rs.getString("name"));
            account.setBalance(rs.getBigDecimal("amount"));
            account.setActive(rs.getBoolean("is_active"));
            account.setDeblockingQuery(rs.getBoolean("deblock_query"));
            account.setUserID(rs.getLong("users_id"));
            accounts.add(account);
        }
        return accounts;
    }

    @Override
    public Account getResult(ResultSet rs) throws SQLException {
        Account account = new Account();
        while (rs.next()) {
            account.setId(rs.getLong("id"));
            account.setNumber(rs.getLong("number"));
            account.setName(rs.getString("name"));
            account.setBalance(rs.getBigDecimal("amount"));
            account.setActive(rs.getBoolean("is_active"));
            account.setDeblockingQuery(rs.getBoolean("deblock_query"));
            account.setUserID(rs.getLong("users_id"));
        }
        return account;
    }
}
