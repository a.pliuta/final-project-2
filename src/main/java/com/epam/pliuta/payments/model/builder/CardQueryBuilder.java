package com.epam.pliuta.payments.model.builder;

import com.epam.pliuta.payments.model.entity.Card;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author: Oleksandr Pliuta
 * @date: 02.06.2022
 */
public class CardQueryBuilder extends QueryBuilder<Card> {
    @Override
    public List<Card> getListOfResult(ResultSet rs) throws SQLException {
        List<Card> cards = new ArrayList<>();
        while (rs.next()) {
            Card card = new Card();
            card.setId(rs.getLong("id"));
            card.setNumber(rs.getString("card_number"));
            card.setEndDate(rs.getDate("end_date"));
            card.setAccountID(rs.getLong("accounts_id"));
            card.setUserID(rs.getLong("users_id"));
            cards.add(card);
        }
        return cards;
    }

    @Override
    public Card getResult(ResultSet rs) throws SQLException {
        Card card = new Card();
        while (rs.next()) {
            card.setId(rs.getLong("id"));
            card.setNumber(rs.getString("card_number"));
            card.setEndDate(rs.getDate("end_date"));
            card.setAccountID(rs.getLong("accounts_id"));
            card.setUserID(rs.getLong("users_id"));
        }
        return card;
    }
}

