package com.epam.pliuta.payments.model.repository;

import com.epam.pliuta.payments.model.entity.Account;
import com.epam.pliuta.payments.model.entity.Card;

import java.util.List;

/**
 * @author: Oleksandr Pliuta
 * @date: 31.05.2022
 */

public interface ICardRepo extends IEntityRepo<Card> {

    long getNextIdValue();

    List<Card> getByAccountID(long accountId);
    List<Card> getByUserID(long userId);
}
