package com.epam.pliuta.payments.model.repository.impl;

import com.epam.pliuta.payments.model.builder.PaymentQueryBuilder;
import com.epam.pliuta.payments.model.builder.QueryBuilder;
import com.epam.pliuta.payments.model.connectionpool.DBManager;
import com.epam.pliuta.payments.model.entity.Payment;
import com.epam.pliuta.payments.model.repository.IPaymentRepo;
import com.epam.pliuta.payments.util.PaginationSort;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * @author: Oleksandr Pliuta
 * @date: 02.06.2022
 */
public class PaymentRepoImpl implements IPaymentRepo {
    private static final String CREATE = "INSERT INTO payments (id, number, sum, is_sent, fill_date, sent_date, accounts_id, cards_id, users_id) "
            + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String GET_ALL = "SELECT * FROM payments";
    private static final String GET_ALL_LIMIT = "SELECT * FROM payments LIMIT ? , ?";
    private static final String GET_ALL_LIMIT_SORT_NUMBER_ASC = "SELECT * FROM payments ORDER BY number ASC LIMIT ? , ?";
    private static final String GET_ALL_LIMIT_SORT_NUMBER_DESC = "SELECT * FROM payments ORDER BY number DESC LIMIT ? , ?";
    private static final String GET_ALL_LIMIT_SORT_FDATE_ASC = "SELECT * FROM payments ORDER BY fill_date ASC LIMIT ? , ?";
    private static final String GET_ALL_LIMIT_SORT_FDATE_DESC = "SELECT * FROM payments ORDER BY fill_date DESC LIMIT ? , ?";
    private static final String GET_ALL_LIMIT_USERID = "SELECT * FROM payments WHERE users_id = ? LIMIT ? , ?";
    private static final String GET_ALL_LIMIT_SORT_NUMBER_ASC_USERID = "SELECT * FROM payments WHERE users_id = ? ORDER BY number ASC LIMIT ? , ?";
    private static final String GET_ALL_LIMIT_SORT_NUMBER_DESC_USERID = "SELECT * FROM payments WHERE users_id = ? ORDER BY number DESC LIMIT ? , ?";
    private static final String GET_ALL_LIMIT_SORT_FDATE_ASC_USERID = "SELECT * FROM payments WHERE users_id = ? ORDER BY fill_date ASC LIMIT ? , ?";
    private static final String GET_ALL_LIMIT_SORT_FDATE_DESC_USERID = "SELECT * FROM payments WHERE users_id = ? ORDER BY fill_date DESC LIMIT ? , ?";

    //private static final String GET_BY_ID = "SELECT number, sum, is_sent, fill_date, sent_date, accounts_id FROM payments WHERE id = ?";
    private static final String GET_BY_ID = "SELECT * FROM payments WHERE id = ?";
    private static final String GET_BY_ACCOUNT_ID = "SELECT * FROM payments WHERE accounts_id = ?";
    private static final String UPDATE = "UPDATE payments SET number = ?, sum = ?, is_sent = ?, fill_date = ?," +
            " sent_date = ?, accounts_id = ? , cards_id = ?, users_id = ? WHERE id = ?";
    private static final String DELETE = "DELETE FROM payments WHERE id = ?";
    private static final String GET_NEXT_AUTO_INCREMENT = "SELECT MAX(id)+1 FROM payments";
    private static final String GET_MAX_ID = "SELECT MAX(id) FROM payments";

    private static final String GET_SIZE = "SELECT count(*) FROM payments";
    private static final String GET_SIZE_BY_ID = "SELECT count(*) FROM payments WHERE users_id = ?";
    private DBManager instance = DBManager.getInstance();
    private QueryBuilder queryBuilder = new PaymentQueryBuilder();

    @Override
    public List<Payment> getAll() {
        return this.queryBuilder.executeAndReturnList(instance, GET_ALL);
    }

    @Override
    public List<Payment> getAllLimitSort(long from, long size, String sortType) {

        switch(sortType) {
            case PaginationSort.NUMBER_STRAIGHT:
                return this.queryBuilder.executeAndReturnList(instance, GET_ALL_LIMIT_SORT_NUMBER_ASC, from, size);
            case PaginationSort.NUMBER_FORWARD:
                return this.queryBuilder.executeAndReturnList(instance, GET_ALL_LIMIT_SORT_NUMBER_DESC, from, size);
            case PaginationSort.DATE_STRAIGHT:
                return this.queryBuilder.executeAndReturnList(instance, GET_ALL_LIMIT_SORT_FDATE_ASC, from, size);
            case PaginationSort.DATE_FORWARD:
                return this.queryBuilder.executeAndReturnList(instance, GET_ALL_LIMIT_SORT_FDATE_DESC, from, size);
            case PaginationSort.WITHOUT_SORT:
            default:
                return this.queryBuilder.executeAndReturnList(instance, GET_ALL_LIMIT, from, size);
        }
    }

    @Override
    public Payment getById(long id) {
        return (Payment) this.queryBuilder.executeAndReturn(instance, GET_BY_ID, id);
    }
    @Override
    public List<Payment> getByAccountID(long accountId) {
        return this.queryBuilder.executeAndReturnList(instance, GET_BY_ACCOUNT_ID, accountId);
    }
    @Override
    public List<Payment> getByUserIDLimitSort(long user_id, long from, long size, String sortType) {

        switch(sortType) {
            case PaginationSort.NUMBER_STRAIGHT:
                return this.queryBuilder.executeAndReturnList(instance, GET_ALL_LIMIT_SORT_NUMBER_ASC_USERID, user_id, from, size);
            case PaginationSort.NUMBER_FORWARD:
                return this.queryBuilder.executeAndReturnList(instance, GET_ALL_LIMIT_SORT_NUMBER_DESC_USERID, user_id, from, size);
            case PaginationSort.DATE_STRAIGHT:
                return this.queryBuilder.executeAndReturnList(instance, GET_ALL_LIMIT_SORT_FDATE_ASC_USERID, user_id, from, size);
            case PaginationSort.DATE_FORWARD:
                return this.queryBuilder.executeAndReturnList(instance, GET_ALL_LIMIT_SORT_FDATE_DESC_USERID, user_id, from, size);
            case PaginationSort.WITHOUT_SORT:
            default:
                return this.queryBuilder.executeAndReturnList(instance, GET_ALL_LIMIT_USERID, user_id, from, size);
        }
    }

    @Override
    public void create(Payment payment) {
        long id = queryBuilder.getNextAutoIncrement(instance, GET_NEXT_AUTO_INCREMENT);
        this.queryBuilder.execute(instance, CREATE, id, payment.getNumber(), payment.getSum(), payment.isSent(),
                payment.getFillDate(), payment.getSentDate(), payment.getAccountID(), payment.getCardID(), payment.getUserID());;
    }

    @Override
    public void update(Payment payment) {
        this.queryBuilder.execute(instance, UPDATE, payment.getNumber(), payment.getSum(), payment.isSent(),
                payment.getFillDate(), payment.getSentDate(), payment.getAccountID(), payment.getCardID(), payment.getUserID(), payment.getId());
    }

    @Override
    public void delete(long id) {
        this.queryBuilder.execute(instance, DELETE, id);
    }

    @Override
    public long newNumberPayment() {
        long payNumber = 0;
        long id = queryBuilder.getNextAutoIncrement(instance, GET_MAX_ID);
        Payment payment = getById(id);

        if (payment != null) {
            payNumber = 1 + payment.getNumber();
        }

        return payNumber;
    }

    @Override

    public long getNextIdValue() {
        return queryBuilder.getNextAutoIncrement(instance, GET_NEXT_AUTO_INCREMENT);
    }

    @Override
    public long  getSize(){ return queryBuilder.executeGetSize(instance, GET_SIZE);}
    @Override
    public long  getSizeByUserID(long userID){ return queryBuilder.executeGetSizeById(instance, GET_SIZE_BY_ID, userID);}

    @Override
    public void createTransactionEnd(Payment payment,  Connection conn){
        long id = queryBuilder.getNextAutoIncrement(instance, GET_NEXT_AUTO_INCREMENT);
        this.queryBuilder.executeTransactionEnd(instance, conn, CREATE, id, payment.getNumber(), payment.getSum(), payment.isSent(),
                payment.getFillDate(), payment.getSentDate(), payment.getAccountID(), payment.getCardID(), payment.getUserID());;
    }
    @Override
    public void updateTransactionEnd(Payment payment,  Connection conn){
        //long id = queryBuilder.getNextAutoIncrement(instance, GET_NEXT_AUTO_INCREMENT);
        this.queryBuilder.executeTransactionEnd(instance, conn, UPDATE, payment.getNumber(), payment.getSum(), payment.isSent(),
                payment.getFillDate(), payment.getSentDate(), payment.getAccountID(), payment.getCardID(), payment.getUserID(), payment.getId());;
    }
}
