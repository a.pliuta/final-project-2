package com.epam.pliuta.payments.model.service;

import com.epam.pliuta.payments.model.entity.Payment;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * @author: Oleksandr Pliuta
 * @date: 02.06.2022
 */
public interface IPaymentService{
    List<Payment> findAll();
    //List<Payment> findAllLimit(long from, long size);
    List<Payment> findAllLimitSort(long from, long size, String sortType);
    List<Payment> findAllByUserIDLimitSort(long user_id, long from, long size, String sortType);

    Payment find(long id);

    void save(Payment payment);

    void update(Payment payment);

    void remove(int id);

    List<Payment> findByAccountID(long accountID);

    long findSize();

    long findSizeByUserID(long userID);
    long findNextNumber();
    void saveTransactionEnd(Payment payment, Connection conn);
    void updateTransactionEnd(Payment payment, Connection conn);

}
