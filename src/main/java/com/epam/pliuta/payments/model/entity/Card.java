package com.epam.pliuta.payments.model.entity;

import java.sql.Date;
import java.util.Objects;

/**
 * @author: Oleksandr Pliuta
 * @date: 31.05.2022
 */
public class Card extends Entity {
    private String number;
    private Date endDate;

    private long accountID;

    private long accountNumber;

    private long userID;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public long getAccountID() {
        return accountID;
    }

    public void setAccountID(long accountID) {
        this.accountID = accountID;
    }

    public long getAccountNumber() {return accountNumber;}

    public void setAccountNumber(long accountNumber) {this.accountNumber = accountNumber;}

    public long getUserID() {return userID;}

    public void setUserID(long userID) {this.userID = userID;}

    @Override
    public String toString() {
        return "\n      Card{" +
                "number='" + number + '\'' +
                ", endDate=" + endDate +
                ", accountID=" + accountID +
                ", accountNumber=" + accountNumber +
                ", userID=" + userID +
                '}';
    }
}

