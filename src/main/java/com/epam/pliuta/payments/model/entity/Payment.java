package com.epam.pliuta.payments.model.entity;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * @author: Oleksandr Pliuta
 * @date: 31.05.2022
 */
public class Payment extends Entity {
    private long number;
    private BigDecimal sum;
    private boolean sent;
    private LocalDateTime fillDate;
    private LocalDateTime sentDate;
    private long accountID;
    private long cardID;
    private String cardNumber;
    private long accountNumber;

    private long userID;

    public Payment() {
    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }

    public boolean isSent() {
        return sent;
    }

    public void setSent(boolean sent) {
        this.sent = sent;
    }

    public LocalDateTime getFillDate() {
        return fillDate;
    }

    public void setFillDate(LocalDateTime fillDate) {
        this.fillDate = fillDate;
    }

    public LocalDateTime getSentDate() {
        return sentDate;
    }

    public void setSentDate(LocalDateTime sentDate) {
        this.sentDate = sentDate;
    }

    public long getAccountID() {
        return accountID;
    }

    public void setAccountID(long accountID) {
        this.accountID = accountID;
    }

    public long getCardID() {return cardID;}

    public void setCardID(long cardID) {this.cardID = cardID;}

    public String getCardNumber() {return cardNumber;}

    public void setCardNumber(String cardNumber) {this.cardNumber = cardNumber;}

    public long getAccountNumber() {return accountNumber;}

    public void setAccountNumber(long accountNumber) {this.accountNumber = accountNumber;}

    public long getUserID() {return userID;}

    public void setUserID(long userID) {this.userID = userID;}

    @Override
    public String toString() {
        return "\n              Payment{" +
                "number=" + number +
                ", sum=" + sum +
                ", sent=" + sent +
                ", fillDate=" + fillDate +
                ", sentDate=" + sentDate +
                ", accountID=" + accountID +
                ", cardID=" + cardID +
                ", cardNumber='" + cardNumber + '\'' +
                ", accountNumber=" + accountNumber +
                ", userID=" + userID +
                '}';
    }
}
