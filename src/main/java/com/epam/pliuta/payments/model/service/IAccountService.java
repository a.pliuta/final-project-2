package com.epam.pliuta.payments.model.service;

import com.epam.pliuta.payments.model.entity.Account;
import com.epam.pliuta.payments.model.entity.Payment;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * @author: Oleksandr Pliuta
 * @date: 01.06.2022
 */
public interface IAccountService {
    List<Account> findAll();

    List<Account> findAllLimitSort(long from, long size, String sortType);

    List<Account> findAllByUserIDLimitSort(long user_id, long from, long size, String sortType);

    List<Account> findAllFullInfo();

    Account findByIDFullInfo(long id);

    Account findByNumberFullInfo(long number);

    Account find(long id);

    void save(Account account);

    void update(Account account);

    void remove(int id);

    List<Account> findByUserID(long userID);
    List<Account> findAllByUserID(long userID);

    long findSize();
    long findSizeByUserID(long userID);
    long findNextNumber();

    Connection updateTransactionStart(Account account);

}
