package com.epam.pliuta.payments.model.service.impl;

import com.epam.pliuta.payments.model.entity.Payment;
import com.epam.pliuta.payments.model.repository.IPaymentRepo;
import com.epam.pliuta.payments.model.service.IPaymentService;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * @author: Oleksandr Pliuta
 * @date: 02.06.2022
 */
public class PaymentServiceImpl implements IPaymentService {

    private final IPaymentRepo repo;

    public PaymentServiceImpl(IPaymentRepo repo) {
        this.repo = repo;
    }

    @Override
    public List<Payment> findAll() {
        return this.repo.getAll();
    }
//    @Override
//    public List<Payment> findAllLimit(long from, long size) {
//        return this.repo.getAllLimit(from,size);
//    }
    @Override
    public List<Payment> findAllLimitSort(long from, long size, String sortType){
        return this.repo.getAllLimitSort(from, size, sortType);
    }
    @Override
    public List<Payment> findAllByUserIDLimitSort(long user_id, long from, long size, String sortType){
        return this.repo.getByUserIDLimitSort(user_id, from, size, sortType);
    }
    @Override
    public Payment find(long id) {
        return this.repo.getById(id);
    }

    @Override
    public void save(Payment payment) {
        this.repo.create(payment);
    }

    @Override
    public void update(Payment payment) {this.repo.update(payment);}

    @Override
    public void remove(int id) {
        this.repo.delete(id);
    }

    @Override
    public List<Payment> findByAccountID(long accountID) {
        return this.repo.getByAccountID(accountID);
    }
    @Override
    public long findSize() {
        return this.repo.getSize();
    }
    @Override
    public long findSizeByUserID(long userID) {
        return this.repo.getSizeByUserID(userID);
    }
    @Override
    public long findNextNumber() {return this.repo.newNumberPayment();}
    @Override
    public void saveTransactionEnd(Payment payment,Connection conn){
        this.repo.createTransactionEnd(payment, conn);
    }
    @Override
    public void updateTransactionEnd(Payment payment, Connection conn){
        this.repo.updateTransactionEnd(payment, conn);
    }

}
