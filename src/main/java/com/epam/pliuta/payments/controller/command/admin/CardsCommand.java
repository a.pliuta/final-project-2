package com.epam.pliuta.payments.controller.command.admin;

import com.epam.pliuta.payments.appcontext.AppContext;
import com.epam.pliuta.payments.controller.Path;
import com.epam.pliuta.payments.controller.command.ICommand;
import com.epam.pliuta.payments.model.entity.Account;
import com.epam.pliuta.payments.model.entity.Card;
import com.epam.pliuta.payments.model.entity.Payment;
import com.epam.pliuta.payments.model.service.IAccountService;
import com.epam.pliuta.payments.model.service.ICardService;
import com.epam.pliuta.payments.model.service.IPaymentService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;

/**
 * @author: Oleksandr Pliuta
 * @date: 05.06.2022
 */
public class CardsCommand  implements ICommand {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        IAccountService accountService = AppContext.getInstance().getAccountService();
        ICardService cardService = AppContext.getInstance().getCardService();

        List<Card> cards = cardService.findAll();

        List<Account> accounts = accountService.findAll();
        HashMap<Long, Account> accountsHashMap = new HashMap<>();
        for (Account account : accounts) {
            accountsHashMap.put(account.getId(), account);
        }
        for (Card card : cards) {
            card.setAccountNumber(accountsHashMap.get(card.getAccountID()).getNumber());
        }

        request.setAttribute("fullCard", cards);

        return Path.PAGE_CARDS;
    }
}
