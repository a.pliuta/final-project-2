package com.epam.pliuta.payments.controller.command.admin;

import com.epam.pliuta.payments.appcontext.AppContext;
import com.epam.pliuta.payments.controller.Path;
import com.epam.pliuta.payments.controller.command.ICommand;
import com.epam.pliuta.payments.model.entity.*;
import com.epam.pliuta.payments.model.service.IAccountService;
import com.epam.pliuta.payments.model.service.ICardService;
import com.epam.pliuta.payments.model.service.IPaymentService;
import com.epam.pliuta.payments.util.App;
import com.epam.pliuta.payments.util.PaginationSort;
import com.epam.pliuta.payments.util.RequestUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;

import static com.epam.pliuta.payments.controller.Path.PAGE_PARAMETER;
import static com.epam.pliuta.payments.controller.Path.SORT_PARAMETER;

/**
 *
 * @author: Oleksandr Pliuta
 * @date: 04.06.2022
 */
public class PaymentsCommand implements ICommand {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        IPaymentService paymentService = AppContext.getInstance().getPaymentService();
        IAccountService accountService = AppContext.getInstance().getAccountService();
        ICardService cardService = AppContext.getInstance().getCardService();

        PaginationSort ps = new PaginationSort();
        ps.setSortType(RequestUtils.getStringParameter(request, SORT_PARAMETER));
        ps.setCurrentPage(RequestUtils.getIntParameter(request, PAGE_PARAMETER));
        ps.setNumberOfItems(paymentService.findSize());
        ps.calc();

        List <Payment> payments= paymentService.findAllLimitSort(ps.getStartFrom(),ps.MAX_ITEM_ON_PAGE, ps.getSortType());

        List<Card> cards = cardService.findAll();
        HashMap<Long, Card> cardsHashMap = new HashMap<>();
        for (Card card : cards) {
            cardsHashMap.put(card.getId(), card);
        }
        List<Account> accounts = accountService.findAll();
        HashMap<Long, Account> accountsHashMap = new HashMap<>();
        for (Account account : accounts) {
            accountsHashMap.put(account.getId(), account);
        }
        for (Payment payment : payments) {
            payment.setCardNumber(cardsHashMap.get(payment.getCardID()).getNumber());
            payment.setAccountNumber(accountsHashMap.get(payment.getAccountID()).getNumber());
        }

        request.setAttribute("fullPayment", payments);
        request.setAttribute("ps", ps);

        return Path.PAGE_PAYMENTS;
    }
}
