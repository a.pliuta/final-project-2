package com.epam.pliuta.payments.controller.command.client;

import com.epam.pliuta.payments.appcontext.AppContext;
import com.epam.pliuta.payments.controller.Path;
import com.epam.pliuta.payments.controller.command.ICommand;
import com.epam.pliuta.payments.model.entity.Account;
import com.epam.pliuta.payments.model.entity.Card;
import com.epam.pliuta.payments.model.entity.Payment;
import com.epam.pliuta.payments.model.entity.User;
import com.epam.pliuta.payments.model.service.IAccountService;
import com.epam.pliuta.payments.model.service.ICardService;
import com.epam.pliuta.payments.model.service.IPaymentService;
import com.epam.pliuta.payments.model.service.IUserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author: Oleksandr Pliuta
 * @date: 06.06.2022
 */
public class CardCommand implements ICommand {

    private final IUserService userService =  AppContext.getInstance().getUserService();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        String forward = Path.PAGE_CARD;

        User fullUser = (User) session.getAttribute("user");

        List<Account> userAccounts = userService.findByIDFullInfo(fullUser.getId()).getAccounts();
        List<Card> userCards = new ArrayList<>();
        for(Account account : userAccounts){
            for (Card card : account.getCards()){
                userCards.add(card);
            }
        }
        List<Payment> userPayments = new ArrayList<>();
        for(Account account : userAccounts){
            for (Payment payment : account.getPayments()){
                userPayments.add(payment);
            }
        }

        HashMap<Long, Account> accountsHashMap = new HashMap<>();
        for (Account account : userAccounts) {
            accountsHashMap.put(account.getId(), account);
        }
        for (Card card : userCards) {
            card.setAccountNumber(accountsHashMap.get(card.getAccountID()).getNumber());
        }

        HashMap<Long, Card> cardsHashMap = new HashMap<>();
        for (Card card : userCards) {
            cardsHashMap.put(card.getId(), card);
        }

        for (Payment payment : userPayments) {
            payment.setCardNumber(cardsHashMap.get(payment.getCardID()).getNumber());
            payment.setAccountNumber(accountsHashMap.get(payment.getCardID()).getNumber());
        }


        request.setAttribute("userAccounts", userAccounts);
        request.setAttribute("userCards", userCards);
        request.setAttribute("userPayments", userPayments);

        return forward;
    }
}
