package com.epam.pliuta.payments.controller.command.client;

import com.epam.pliuta.payments.appcontext.AppContext;
import com.epam.pliuta.payments.controller.Path;
import com.epam.pliuta.payments.controller.command.ICommand;
import com.epam.pliuta.payments.model.entity.Account;
import com.epam.pliuta.payments.model.entity.User;
import com.epam.pliuta.payments.model.service.IAccountService;
import com.epam.pliuta.payments.model.service.IUserService;
import com.epam.pliuta.payments.util.Validator;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author: Oleksandr Pliuta
 * @date: 06.06.2022
 */
public class EditAccountCommand implements ICommand {
    IAccountService accountService = AppContext.getInstance().getAccountService();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        long id = Long.parseLong(request.getParameter("account_id"));
        String errorMessage;
        String forward = Path.COMMAND_ACCOUNT;

        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        Account account;
        if (id != 0) {
            account = accountService.findByIDFullInfo(id);
            ServletContext servletContext = request.getServletContext();
            servletContext.setAttribute("account_id", account.getId());
        } else {
            account = new Account();
            if (request.getParameter("btnAddNew") != null) {

                String tempString = request.getParameter("account_name");
                errorMessage = Validator.validateSentences(tempString, "account_name", 45);
                if (errorMessage != null) {
                    request.setAttribute("errorMessage", errorMessage);
                    return forward;
                }

                account.setNumber(accountService.findNextNumber());
                account.setName(tempString);
                account.setBalance(BigDecimal.valueOf(0));
                account.setActive(false);
                account.setDeblockingQuery(true);
                account.setUserID(user.getId());
                forward = addNewAccount(request, response, accountService, account);
                ServletContext servletContext = request.getServletContext();
                servletContext.setAttribute("account_id", account.getId());
            }
            if (request.getParameter("btnAddSum") != null) {

                String tempString = request.getParameter("account_number");
                errorMessage = Validator.validateIntNumber(tempString);
                if (errorMessage != null) {
                    request.setAttribute("errorMessage", errorMessage);
                    return forward;
                }
                long accountNumber = Integer.parseInt(tempString);

                List<Account> userAccounts = accountService.findByUserID(user.getId());
                for (Account tempAccount : userAccounts) {
                    if (tempAccount.getNumber() == accountNumber) {
                        account = tempAccount;
                        break;
                    }
                }
                if (account.getId() != 0) {
                    tempString = request.getParameter("account_addsum");
                    errorMessage = Validator.validateAmmount(tempString);
                    if (errorMessage != null) {
                        request.setAttribute("errorMessage", errorMessage);
                        return forward;
                    }
                    account.setBalance(account.getBalance().add(BigDecimal.valueOf(Integer.parseInt(tempString))));
                    forward = update(request, response, accountService, account);
                    ServletContext servletContext = request.getServletContext();
                    servletContext.setAttribute("account_id", account.getId());
                } else {
                    errorMessage = "Wrong an account number";
                    request.setAttribute("errorMessage", errorMessage);
                    return forward;
                }
            }
        }

        if (request.getParameter("btnLock") != null) {
            forward = blockAccount(response, accountService, account);
        }
        if (request.getParameter("btnDeBlock") != null) {
            forward = setDeblockQuery(response, accountService, account);
        }


        return forward;
    }

    private String update(HttpServletRequest request, HttpServletResponse response,
                          IAccountService accountService, Account account) {
        String resp = Path.COMMAND_ACCOUNT;

        if (account.isActive()) {
            accountService.update(account);
        } else {
            String errorMessage;
            errorMessage = "An account is not active";
            request.setAttribute("errorMessage", errorMessage);
            return resp;
        }

        HttpSession session = request.getSession();
        session.removeAttribute("newAccount");
        session.setAttribute("newAccount", account);
        try {
            response.sendRedirect(resp);
            resp = Path.COMMAND_REDIRECT;
        } catch (IOException e) {
            resp = Path.PAGE_ERROR_PAGE;
        }
        return resp;
    }

    private String addNewAccount(HttpServletRequest request, HttpServletResponse response,
                                 IAccountService accountService, Account account) {
        String resp = Path.COMMAND_ACCOUNT;

        accountService.save(account);

        HttpSession session = request.getSession();
        session.removeAttribute("newAccount");
        session.setAttribute("newAccount", account);
        try {
            response.sendRedirect(resp);
            resp = Path.COMMAND_REDIRECT;
        } catch (IOException e) {
            resp = Path.PAGE_ERROR_PAGE;
        }
        return resp;
    }

    private String blockAccount(HttpServletResponse response, IAccountService accountService, Account account) {
        String resp = Path.COMMAND_ACCOUNT;
        if (account.isActive()) {
            account.setActive(false);
            accountService.update(account);
        }
        try {
            response.sendRedirect(resp);
            resp = Path.COMMAND_REDIRECT;
        } catch (IOException e) {
            resp = Path.PAGE_ERROR_PAGE;
        }
        return resp;
    }

    private String setDeblockQuery(HttpServletResponse response, IAccountService accountService, Account account) {
        String resp = Path.COMMAND_ACCOUNT;
        if (account.isActive() == false) {
            account.setDeblockingQuery(true);
            accountService.update(account);
        }
        try {
            response.sendRedirect(resp);
            resp = Path.COMMAND_REDIRECT;
        } catch (IOException e) {
            resp = Path.PAGE_ERROR_PAGE;
        }
        return resp;
    }
}


