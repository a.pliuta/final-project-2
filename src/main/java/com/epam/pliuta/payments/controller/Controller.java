package com.epam.pliuta.payments.controller;

import com.epam.pliuta.payments.controller.command.CommandFactory;
import com.epam.pliuta.payments.controller.command.ICommand;
import com.epam.pliuta.payments.exception.AppException;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 *
 * * Main Controller Servlet.
 *
 * @author: Oleksandr Pliuta
 * @date: 03.06.2022
 */
//@WebServlet(name = "app", value = "/app")
public class Controller extends HttpServlet {
    private static final Logger LOG = Logger.getLogger(Controller.class);


    public void init() {}
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        LOG.info("doGet Servlet");
        process(request, response);
    }
    @Override
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {
        LOG.info("doPost Servlet");
        process(request, response);
    }


    private void process(HttpServletRequest request,
                         HttpServletResponse response) throws IOException, ServletException {

        LOG.debug("Controller starts");
        // extract command name from the request
        String commandName = request.getParameter("action");
        LOG.trace("Request parameter: command --> " + commandName);

        CommandFactory commandFactory = CommandFactory.commandFactory();
        ICommand iCommand = commandFactory.getCommand(request);
        LOG.trace("Obtained command --> " + iCommand);

        String page = iCommand.execute(request, response);
        RequestDispatcher dispatcher = request.getRequestDispatcher(page);
        if (!page.equals("redirect")) {
            dispatcher.forward(request, response);
        }
    }
}


