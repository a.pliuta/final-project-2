package com.epam.pliuta.payments.controller.command.client;

import com.epam.pliuta.payments.appcontext.AppContext;
import com.epam.pliuta.payments.controller.Path;
import com.epam.pliuta.payments.controller.command.ICommand;
import com.epam.pliuta.payments.model.entity.Account;
import com.epam.pliuta.payments.model.entity.Card;
import com.epam.pliuta.payments.model.entity.Payment;
import com.epam.pliuta.payments.model.entity.User;
import com.epam.pliuta.payments.model.service.IAccountService;
import com.epam.pliuta.payments.model.service.ICardService;
import com.epam.pliuta.payments.model.service.IPaymentService;
import com.epam.pliuta.payments.model.service.IUserService;
import com.epam.pliuta.payments.util.Validator;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author: Oleksandr Pliuta
 * @date: 06.06.2022
 */
public class EditCardCommand implements ICommand {
    private final IUserService userService =  AppContext.getInstance().getUserService();
    IAccountService accountService = AppContext.getInstance().getAccountService();
    ICardService cardService = AppContext.getInstance().getCardService();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        long id = Long.parseLong(request.getParameter("card_id"));
        String errorMessage;
        String forward = Path.COMMAND_CARD;

        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        Card card;
        if (id != 0) {
            card = cardService.find(id);
            ServletContext servletContext = request.getServletContext();
            servletContext.setAttribute("card_id", card.getId());
        } else {
            card = new Card();
            String tempString = request.getParameter("cc-number");
            errorMessage = Validator.validateCardNumber(tempString);
            if (errorMessage != null) {
                request.setAttribute("errorMessage", errorMessage);
                return forward;
            }
            card.setNumber(tempString);

            if (request.getParameter("btnAddCard") != null) {
                tempString = request.getParameter("cc-expiration");
                errorMessage = Validator.validateDate(tempString);
                if (errorMessage != null) {
                    request.setAttribute("errorMessage", errorMessage);
                    return forward;
                }
                card.setEndDate(Date.valueOf(tempString));

                tempString = request.getParameter("account_number");
                errorMessage = Validator.validateIntNumber(tempString);
                if (errorMessage != null) {
                    request.setAttribute("errorMessage", errorMessage);
                    return forward;
                }
                long accountNumber = Integer.parseInt(tempString);

                List<Account> userAccounts = accountService.findByUserID(user.getId());
                Account cardAccount = new Account();
                for (Account account : userAccounts) {
                    if (account.getNumber() == accountNumber) {
                        cardAccount = account;
                        break;
                    }
                }
                if (cardAccount.getId() != 0) {
                    card.setAccountID(cardAccount.getId());
                    card.setUserID(user.getId());
                    forward = addNewCard(request, response, cardService, card);
                    ServletContext servletContext = request.getServletContext();
                    servletContext.setAttribute("card_id", card.getId());
                } else {
                    errorMessage = "Wrong the account number";
                    request.setAttribute("errorMessage", errorMessage);
                    return forward;
                }
            }
            if (request.getParameter("btnPayCard") != null) {
                List<Account> userAccounts = userService.findByIDFullInfo(user.getId()).getAccounts();
                Account payAccount = new Account();
                for(Account account : userAccounts){
                    for (Card tCard : account.getCards()){
                        if (tCard.getNumber().equals(card.getNumber())){
                            payAccount = account;
                            card = tCard;
                        }
                    }
                }
                if (payAccount.getId() != 0) {
                    forward = payByCard(request, response, accountService, payAccount, card);
                    ServletContext servletContext = request.getServletContext();
                    servletContext.setAttribute("card_id", card.getId());
                    session.removeAttribute("newCard");
                    session.setAttribute("newCard", card);
                } else {
                    errorMessage = "Wrong the card number";
                    request.setAttribute("errorMessage", errorMessage);
                    return forward;
                }
            }

        }

        return forward;
    }

    private String addNewCard(HttpServletRequest request, HttpServletResponse response,
                          ICardService cardService, Card card) {
        String resp = Path.COMMAND_CARD;

        cardService.save(card);

        HttpSession session = request.getSession();
        session.removeAttribute("newCard");
        session.setAttribute("newCard", card);
        try {
            response.sendRedirect(resp);
            resp = Path.COMMAND_REDIRECT;
        } catch (IOException e) {
            resp = Path.PAGE_ERROR_PAGE;
        }
        return resp;
    }

    private String payByCard(HttpServletRequest request, HttpServletResponse response,
                             IAccountService accountService, Account account, Card card){
        String errorMessage;
        String resp = Path.COMMAND_CARD;
        final IPaymentService paymentService =  AppContext.getInstance().getPaymentService();

        if (account.isActive()==true){
            String tempString;
            tempString = request.getParameter("payAmount");
            errorMessage = Validator.validateAmmount(tempString);
            if (errorMessage != null) {
                request.setAttribute("errorMessage", errorMessage);
                return resp;
            }
            BigDecimal amount = BigDecimal.valueOf(Integer.parseInt(request.getParameter("payAmount")));
            if (account.getBalance().compareTo(amount) > 0){
                Payment payment = new Payment();
                payment.setNumber( paymentService.findNextNumber());
                payment.setSum(amount);
                payment.setSent(false);
                payment.setFillDate(LocalDateTime.now());
                payment.setSentDate(LocalDateTime.now());
                payment.setAccountID(account.getId());
                payment.setCardID(card.getId());
                payment.setUserID(account.getUserID());
                paymentService.save (payment);
            } else{
                errorMessage = "Lack of money on the account";
                request.setAttribute("errorMessage", errorMessage);
                return resp;
            }
        } else{
            errorMessage = "An account is not active";
            request.setAttribute("errorMessage", errorMessage);
            return resp;
        }

        try {
            response.sendRedirect(resp);
            resp = Path.COMMAND_REDIRECT;
        } catch (IOException e) {
            resp = Path.PAGE_ERROR_PAGE;
        }
        return resp;
    }

    private String blockAccount(HttpServletResponse response, ICardService cardService, Card card) {
        String resp = Path.COMMAND_CARD;
//        if (account.isActive()) {
//            account.setActive(false);
//            accountService.update(account);
//        }
        try {
            response.sendRedirect(resp);
            resp = Path.COMMAND_REDIRECT;
        } catch (IOException e) {
            resp = Path.PAGE_ERROR_PAGE;
        }
        return resp;
    }
    private String setDeblockQuery(HttpServletResponse response, ICardService cardService, Card card) {
        String resp = Path.COMMAND_CARD;
//        if (account.isActive() == false) {
//            account.setDeblockingQuery(true);
//            accountService.update(account);
//        }
        try {
            response.sendRedirect(resp);
            resp = Path.COMMAND_REDIRECT;
        } catch (IOException e) {
            resp = Path.PAGE_ERROR_PAGE;
        }
        return resp;
    }


    private String goBack(HttpServletRequest request, HttpServletResponse response) {
        String resp = Path.COMMAND_CARD;
        HttpSession session = request.getSession();
        session.removeAttribute("newCard");

        ServletContext servletContext = request.getServletContext();
        servletContext.removeAttribute("card_id");
        try {
            response.sendRedirect(resp);
            resp = Path.COMMAND_REDIRECT;
        } catch (IOException e) {
            resp = Path.PAGE_ERROR_PAGE;
        }
        return resp;
    }
}
