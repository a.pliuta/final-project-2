package com.epam.pliuta.payments.controller.command.client;

import com.epam.pliuta.payments.appcontext.AppContext;
import com.epam.pliuta.payments.controller.Path;
import com.epam.pliuta.payments.controller.command.ICommand;
import com.epam.pliuta.payments.model.entity.Account;
import com.epam.pliuta.payments.model.entity.Card;
import com.epam.pliuta.payments.model.entity.Payment;
import com.epam.pliuta.payments.model.entity.User;
import com.epam.pliuta.payments.model.service.IAccountService;
import com.epam.pliuta.payments.model.service.ICardService;
import com.epam.pliuta.payments.model.service.IPaymentService;
import com.epam.pliuta.payments.model.service.IUserService;
import com.epam.pliuta.payments.util.PaginationSort;
import com.epam.pliuta.payments.util.RequestUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.epam.pliuta.payments.controller.Path.PAGE_PARAMETER;
import static com.epam.pliuta.payments.controller.Path.SORT_PARAMETER;

/**
 * @author: Oleksandr Pliuta
 * @date: 06.06.2022
 */
public class PaymentCommand implements ICommand {

    private final IAccountService accountService =  AppContext.getInstance().getAccountService();
    private final ICardService cardService =  AppContext.getInstance().getCardService();
    private final IPaymentService paymentService =  AppContext.getInstance().getPaymentService();



    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        String forward = Path.PAGE_PAYMENT;
        User fullUser = (User) session.getAttribute("user");

        PaginationSort ps = new PaginationSort();
        ps.setSortType(RequestUtils.getStringParameter(request, SORT_PARAMETER));
        ps.setCurrentPage(RequestUtils.getIntParameter(request, PAGE_PARAMETER));
        ps.setNumberOfItems(paymentService.findSizeByUserID(fullUser.getId()));
        ps.calc();

        List <Payment> userPayments= paymentService.findAllByUserIDLimitSort(fullUser.getId(),
                ps.getStartFrom(),ps.MAX_ITEM_ON_PAGE, ps.getSortType());
        List<Account> userAccounts = accountService.findByUserID(fullUser.getId());
        List<Card> userCards = cardService.findByUserID(fullUser.getId());

        HashMap<Long, Account> accountsHashMap = new HashMap<>();
        for (Account account : userAccounts) {
            accountsHashMap.put(account.getId(), account);
        }

        HashMap<Long, Card> cardsHashMap = new HashMap<>();
        for (Card card : userCards) {
            cardsHashMap.put(card.getId(), card);
        }

        for (Payment payment : userPayments) {
            payment.setCardNumber(cardsHashMap.get(payment.getCardID()).getNumber());
            payment.setAccountNumber(accountsHashMap.get(payment.getAccountID()).getNumber());
        }

        request.setAttribute("userPayments", userPayments);
        request.setAttribute("ps", ps);

        return forward;
    }
}
