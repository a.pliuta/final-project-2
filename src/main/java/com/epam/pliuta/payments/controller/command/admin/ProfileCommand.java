package com.epam.pliuta.payments.controller.command.admin;

import com.epam.pliuta.payments.appcontext.AppContext;
import com.epam.pliuta.payments.controller.Path;
import com.epam.pliuta.payments.controller.command.ICommand;
import com.epam.pliuta.payments.model.entity.User;
import com.epam.pliuta.payments.model.service.IUserService;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashSet;

/**
 * @author: Oleksandr Pliuta
 * @date: 04.06.2022
 */
public class ProfileCommand implements ICommand {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        ServletContext servletContext = request.getServletContext();

        if (session.getAttribute("newUser") != null) {
            User newUser = (User) session.getAttribute("newUser");
            request.setAttribute("fullUser", newUser);
        }

        if (request.getParameter("user_id") != null) {
            long id = Long.parseLong(request.getParameter("user_id"));
            show(request, id);
        }

        if (servletContext.getAttribute("user_id") != null) {
            Long id = (Long) servletContext.getAttribute("user_id");
            show(request, id);
        }

        return Path.PAGE_PROFILE;
    }

    private void show(HttpServletRequest request, long id) {
        IUserService userService = AppContext.getInstance().getUserService();

        User user = userService.findByIDFullInfo(id);
        user.setRoleId(user.getRoleId());

        request.setAttribute("fullUser", user);
    }
}

