package com.epam.pliuta.payments.controller.command;

import com.epam.pliuta.payments.controller.command.admin.*;
import com.epam.pliuta.payments.controller.command.client.*;
import com.epam.pliuta.payments.controller.command.login.*;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 *  * Main Command factory for Controller.
 *
 * @author: Oleksandr Pliuta
 * @date: 03.06.2022
 */
public class CommandFactory {
    private static final Logger LOG = Logger.getLogger(CommandFactory.class);

    private static CommandFactory factory = new CommandFactory();
    private static Map<String, ICommand> commands = new HashMap<>();

    private CommandFactory() {}

    /**
     * Singleton.
     */
    public static CommandFactory commandFactory() {
        if (factory == null) {
            factory = new CommandFactory();
        }
        return factory;
    }

    static {

        // common commands
        commands.put("login", new LoginCommand());
        commands.put("logout", new LogoutCommand());
        commands.put("new_client", new NewClientCommand());
        commands.put("i18n", new I18NCommand());
        commands.put("redirect", null);
        commands.put("pdf_builder", new PdfBuilderCommand());
//
//        // admin commands
        commands.put("users", new ShowUserListCommand());
        commands.put("accounts", new AccountsCommand());
        commands.put("cards", new CardsCommand());
        commands.put("registration", new RegistrationCommand());
        commands.put("edit_client", new EditClientCommand());
        commands.put("edit_accounts", new EditAccountsCommand());
        commands.put("profile", new ProfileCommand());
        commands.put("payments", new PaymentsCommand());
//        // client commands
        commands.put("account", new AccountCommand());
        commands.put("edit_account", new EditAccountCommand());
        commands.put("card", new CardCommand());
        commands.put("edit_card", new EditCardCommand());
        commands.put("payment", new PaymentCommand());
        commands.put("edit_payment", new EditPaymentCommand());
        commands.put("user_profile", new UserProfileCommand());
        commands.put("save_profile", new SaveUserProfileCommand());


        LOG.debug("Command container was successfully initialized");
        LOG.trace("Number of commands --> " + commands.size());

    }

    public ICommand getCommand(HttpServletRequest request) {
        String action = request.getParameter("action");
        if (action == null || !commands.containsKey(action)) {
            LOG.trace("Command not found, name --> " + action);
            return commands.get("noCommand");
        }

        return commands.get(action);
    }
}