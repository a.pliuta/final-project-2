package com.epam.pliuta.payments.controller.command.client;

import com.epam.pliuta.payments.appcontext.AppContext;
import com.epam.pliuta.payments.controller.Path;
import com.epam.pliuta.payments.controller.command.ICommand;
import com.epam.pliuta.payments.model.entity.Account;
import com.epam.pliuta.payments.model.entity.Card;
import com.epam.pliuta.payments.model.entity.Payment;
import com.epam.pliuta.payments.model.entity.User;
import com.epam.pliuta.payments.model.service.IAccountService;
import com.epam.pliuta.payments.model.service.ICardService;
import com.epam.pliuta.payments.model.service.IPaymentService;
import com.epam.pliuta.payments.model.service.IUserService;
import com.epam.pliuta.payments.util.ReportBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashSet;

/**
 * @author: Oleksandr Pliuta
 * @date: 26.06.2022
 */
public class PdfBuilderCommand implements ICommand {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        String forward = Path.COMMAND_PAYMENT;

            long id = Long.parseLong(request.getParameter("payment_id"));
            ICardService cardService = AppContext.getInstance().getCardService();
            IUserService userService = AppContext.getInstance().getUserService();
            IPaymentService paymentService =  AppContext.getInstance().getPaymentService();

            Payment payment = paymentService.find(id);
            Card card = cardService.find(payment.getAccountID());
            User user = userService.find(payment.getUserID());

            request.setAttribute("user", user);

            ReportBuilder.paymentPdf(response, payment, user, card);

        return forward;
    }
}