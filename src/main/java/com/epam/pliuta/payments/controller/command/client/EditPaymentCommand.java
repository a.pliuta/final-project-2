package com.epam.pliuta.payments.controller.command.client;

import com.epam.pliuta.payments.appcontext.AppContext;
import com.epam.pliuta.payments.controller.Path;
import com.epam.pliuta.payments.controller.command.ICommand;
import com.epam.pliuta.payments.model.entity.Account;
import com.epam.pliuta.payments.model.entity.Card;
import com.epam.pliuta.payments.model.entity.Payment;
import com.epam.pliuta.payments.model.entity.User;
import com.epam.pliuta.payments.model.service.IAccountService;
import com.epam.pliuta.payments.model.service.ICardService;
import com.epam.pliuta.payments.model.service.IPaymentService;
import com.epam.pliuta.payments.model.service.IUserService;
import com.epam.pliuta.payments.util.Validator;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author: Oleksandr Pliuta
 * @date: 06.06.2022
 */
public class EditPaymentCommand implements ICommand {
    IPaymentService paymentService = AppContext.getInstance().getPaymentService();
    IAccountService accountService = AppContext.getInstance().getAccountService();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        long id = Long.parseLong(request.getParameter("payment_id"));
        String forward = Path.COMMAND_PAYMENT;

        Payment payment;
        if (id != 0) {
            payment = paymentService.find(id);
            ServletContext servletContext = request.getServletContext();
            servletContext.setAttribute("payment_id", payment.getId());
        } else {
            HttpSession session = request.getSession();
            payment = (Payment) session.getAttribute("newPayment");
        }
        if (request.getParameter("btnSent") != null) {
            forward = sendPayment(request, response, paymentService, payment);
            ServletContext servletContext = request.getServletContext();
            servletContext.setAttribute("payment_id", payment.getId());
        }
        return forward;
    }

    private String sendPayment(HttpServletRequest request, HttpServletResponse response,
                          IPaymentService paymentService, Payment payment) {
        String errorMessage;
        String resp = Path.COMMAND_PAYMENT;

        Account account = accountService.find(payment.getAccountID());

        if (payment.isSent() == true) {
            errorMessage = "The payment is sent already";
            request.setAttribute("errorMessage", errorMessage);
            return resp;
        }

        if (account.isActive()==true){
            if (account.getBalance().compareTo(payment.getSum()) >= 0){
                account.setBalance(account.getBalance().subtract(payment.getSum()));
                payment.setSent(true);
                payment.setSentDate(LocalDateTime.now());
                Connection tConn = accountService.updateTransactionStart(account);
                if (tConn != null) {
                    paymentService.updateTransactionEnd (payment, tConn);
                }
            } else{
                errorMessage = "Lack of money on the account";
                request.setAttribute("errorMessage", errorMessage);
                return resp;
            }
        } else{
            errorMessage = "An account is not active";
            request.setAttribute("errorMessage", errorMessage);
            return resp;
        }

        HttpSession session = request.getSession();
        session.setAttribute("payment", payment);
        try {
            response.sendRedirect(resp);
            resp = Path.COMMAND_REDIRECT;
        } catch (IOException e) {
            resp = Path.PAGE_ERROR_PAGE;
        }
        return resp;
    }
}
