package com.epam.pliuta.payments.controller.command.admin;

import com.epam.pliuta.payments.appcontext.AppContext;
import com.epam.pliuta.payments.controller.Path;
import com.epam.pliuta.payments.controller.command.ICommand;
import com.epam.pliuta.payments.model.entity.Account;
import com.epam.pliuta.payments.model.service.IAccountService;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author: Oleksandr Pliuta
 * @date: 06.06.2022
 */
public class EditAccountsCommand implements ICommand {
    IAccountService accountService = AppContext.getInstance().getAccountService();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        long id = Long.parseLong(request.getParameter("account_id"));
        String forward = Path.COMMAND_SHOW_ACCOUNTS;

        Account account;
        if (id != 0) {
            account = accountService.findByIDFullInfo(id);
            ServletContext servletContext = request.getServletContext();
            servletContext.setAttribute("account_id", account.getId());
        } else {
            HttpSession session = request.getSession();
            account = (Account) session.getAttribute("newAccount");
        }

        if (request.getParameter("btnLock") != null) {
            forward = blockAccount(response, accountService, account);
        }

        if (request.getParameter("btnBack") != null) {
            forward = goBack(request, response);
        }
        return forward;
    }


    private String blockAccount(HttpServletResponse response, IAccountService accountService, Account account) {
        String resp = Path.COMMAND_SHOW_ACCOUNTS;
        if (account.isActive()) {
            account.setActive(false);
            accountService.update(account);
        } else {
            account.setActive(true);
            account.setDeblockingQuery(false);
            accountService.update(account);
        }
        try {
            response.sendRedirect(resp);
            resp = Path.COMMAND_REDIRECT;
        } catch (IOException e) {
            resp = Path.PAGE_ERROR_PAGE;
        }
        return resp;
    }

    private String goBack(HttpServletRequest request, HttpServletResponse response) {
        String resp = Path.COMMAND_SHOW_ACCOUNTS;
        HttpSession session = request.getSession();
        session.removeAttribute("newAccount");

        ServletContext servletContext = request.getServletContext();
        servletContext.removeAttribute("account_id");
        try {
            response.sendRedirect(resp);
            resp = Path.COMMAND_REDIRECT;
        } catch (IOException e) {
            resp = Path.PAGE_ERROR_PAGE;
        }
        return resp;
    }
}
