package com.epam.pliuta.payments.controller.command.login;

import com.epam.pliuta.payments.controller.Path;
import com.epam.pliuta.payments.controller.command.ICommand;
import com.epam.pliuta.payments.model.entity.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;

/**
 *  * Internationalization controller command.
 *
 * @author: Oleksandr Pliuta
 * @date: 04.06.2022
 */
public class I18NCommand implements ICommand {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();

        String fmtLocale = "javax.servlet.jsp.jstl.fmt.locale";
        String defaultLocale = "defaultLocale";

        if (request.getParameter("uk") != null) {
            Config.set(session, fmtLocale, Path.LOCALE_NAME_UK);
            session.setAttribute(defaultLocale, "uk");

        } else {
            Config.set(session, fmtLocale, "en");
            session.setAttribute(defaultLocale, Path.LOCALE_NAME_EN);
        }

        User user = (User) session.getAttribute("user");
        if (user == null) return "controller?action=logout";
        return (user.getRoleId() == 1) ? "controller?action=users" : "controller?action=account";
    }
}
