package com.epam.pliuta.payments.controller.command.admin;

import com.epam.pliuta.payments.appcontext.AppContext;
import com.epam.pliuta.payments.controller.Path;
import com.epam.pliuta.payments.controller.command.ICommand;
import com.epam.pliuta.payments.model.entity.User;
import com.epam.pliuta.payments.model.service.IAccountService;
import com.epam.pliuta.payments.model.service.IUserService;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashSet;

/**
 *  * Edit user controller command.
 *
 * @author: Oleksandr Pliuta
 * @date: 04.06.2022
 */
public class EditClientCommand  implements ICommand {
    IUserService userService = AppContext.getInstance().getUserService();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        long id = Long.parseLong(request.getParameter("user_id"));
        String forward = Path.COMMAND_SHOW_USERS;

        User user;
        if (id != 0) {
            user = userService.findByIDFullInfo(id);
            user.setRoleId(user.getRoleId());
            ServletContext servletContext = request.getServletContext();
            servletContext.setAttribute("user_id", user.getId());
        } else {
            HttpSession session = request.getSession();
            user = (User) session.getAttribute("newUser");
        }

        if (request.getParameter("btnLock") != null) {
            forward = blockUser(response, userService, user);
        }

        if (request.getParameter("btnChangeUser") != null) {
            forward = update(request, response, userService, user);
        }

        if (request.getParameter("btnBack") != null) {
            forward = goBack(request, response);
        }
        return forward;
    }

    private String update(HttpServletRequest request, HttpServletResponse response,
                          IUserService userService, User user) {
        String resp = Path.COMMAND_PROFILE;
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        user.setFirstName(firstName);
        user.setLastName(lastName);
        userService.update(user);
        HttpSession session = request.getSession();
        session.removeAttribute("newUser");
        session.setAttribute("newUser", user);
        try {
            response.sendRedirect(resp);
            resp = Path.COMMAND_REDIRECT;
        } catch (IOException e) {
            resp = Path.PAGE_ERROR_PAGE;
        }
        return resp;
    }

    private String blockUser(HttpServletResponse response, IUserService userService, User user) {
        String resp = Path.COMMAND_SHOW_USERS;
        if (user.isActive()) {
            user.setActive(false);
            userService.update(user);
        } else {
            user.setActive(true);
            userService.update(user);
        }
        try {
            response.sendRedirect(resp);
            resp = Path.COMMAND_REDIRECT;
        } catch (IOException e) {
            resp = Path.PAGE_ERROR_PAGE;
        }
        return resp;
    }

    private String goBack(HttpServletRequest request, HttpServletResponse response) {
        String resp = Path.COMMAND_SHOW_USERS;
        HttpSession session = request.getSession();
        session.removeAttribute("newUser");

        ServletContext servletContext = request.getServletContext();
        servletContext.removeAttribute("user_id");
        try {
            response.sendRedirect(resp);
            resp = Path.COMMAND_REDIRECT;
        } catch (IOException e) {
            resp = Path.PAGE_ERROR_PAGE;
        }
        return resp;
    }
}