package com.epam.pliuta.payments.controller;

/**
 *  * Path holder (jsp pages, controller commands).
 *
 * @author: Oleksandr Pliuta
 * @date: 03.06.2022
 */
public class Path {

    public static final String PAGE_WELCOME = "/welcome.jsp";
    public static final String NEW_CLIENT = "/WEB-INF/jsp/unloged/new_client.jsp";
    public static final String PAGE_LOGIN = "/WEB-INF/jsp/unloged/login.jsp";
    public static final String PAGE_ERROR_PAGE = "/WEB-INF/jsp/error_page.jsp";
    public static final String PAGE_PROFILE = "/WEB-INF/jsp/admin/profile.jsp";
    public static final String PAGE_PAYMENTS = "/WEB-INF/jsp/admin/payments.jsp";
    public static final String PAGE_CARDS = "/WEB-INF/jsp/admin/cards.jsp";
    public static final String PAGE_ACCOUNT = "/WEB-INF/jsp/client/account.jsp";
    public static final String PAGE_CARD = "/WEB-INF/jsp/client/card.jsp";
    public static final String PAGE_PAYMENT = "/WEB-INF/jsp/client/payment.jsp";
    public static final String PAGE_ACCOUNTS = "/WEB-INF/jsp/admin/accounts.jsp";
    public static final String PAGE_USER_PROFILE = "/WEB-INF/jsp/client/editclient.jsp";
    public static final String PAGE_USER_LIST = "/WEB-INF/jsp/admin/abonents.jsp";

    // common commands
    public static final String COMMAND_REDIRECT = "redirect";

    public static final String COMMAND_NEW_CLIENT = "controller?action=new_client";

    // admin commands
    public static final String COMMAND_SHOW_USERS = "controller?action=users";
    public static final String COMMAND_SHOW_ACCOUNTS = "controller?action=accounts";
    public static final String COMMAND_PROFILE = "controller?action=profile";

    // client commands
    public static final String COMMAND_ACCOUNT = "controller?action=account";
    public static final String COMMAND_CARD = "controller?action=card";
    public static final String COMMAND_PAYMENT = "controller?action=payment";

    // i18n
    public static final String LOCALE_NAME_UK = "uk";
    public static final String LOCALE_NAME_EN = "en";
    //Pagination
    public static final String SORT_PARAMETER = "sort";
    public static final String PAGE_PARAMETER = "page";

}