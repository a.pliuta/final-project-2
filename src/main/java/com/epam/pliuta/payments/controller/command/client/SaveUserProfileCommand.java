package com.epam.pliuta.payments.controller.command.client;

import com.epam.pliuta.payments.appcontext.AppContext;
import com.epam.pliuta.payments.controller.Path;
import com.epam.pliuta.payments.controller.command.ICommand;
import com.epam.pliuta.payments.model.entity.User;
import com.epam.pliuta.payments.model.service.IUserService;
import com.epam.pliuta.payments.util.Validator;
import org.mindrot.jbcrypt.BCrypt;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *  * User profile save controller command.
 *
 * @author: Oleksandr Pliuta
 * @date: 08.06.2022
 */
public class SaveUserProfileCommand implements ICommand {
    private final IUserService userService = AppContext.getInstance().getUserService();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        String errorMessage;
        String forward = Path.PAGE_USER_PROFILE;

        User user = (User) session.getAttribute("user");

        if (userService.validateAndFillUser(user,request) == true){
            userService.update(user);
        }

        return forward;
    }
}

