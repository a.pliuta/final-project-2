package com.epam.pliuta.payments.controller.command.login;

import com.epam.pliuta.payments.appcontext.AppContext;
import com.epam.pliuta.payments.controller.Path;
import com.epam.pliuta.payments.controller.command.ICommand;
import com.epam.pliuta.payments.model.entity.Role;
import com.epam.pliuta.payments.model.entity.User;
import com.epam.pliuta.payments.model.service.IUserService;
import org.mindrot.jbcrypt.BCrypt;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * @author: Oleksandr Pliuta
 * @date: 04.06.2022
 */
public class RegistrationCommand implements ICommand {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {

        IUserService userService = AppContext.getInstance().getUserService();
        String resp;

        User user = new User();

        if (userService.validateAndFillUser(user,request) == true){
            user.setRoleId(2);
            user.setActive(true);
            userService.save(user);

            HttpSession session = request.getSession();
            session.setAttribute("user", user);
            session.setAttribute("userRole", Role.CLIENT);
            resp = Path.COMMAND_ACCOUNT;
        }else{
            resp = Path.COMMAND_NEW_CLIENT;
        }

        try {
            response.sendRedirect(resp);
            resp = Path.COMMAND_REDIRECT;
        } catch (IOException e) {
            resp = Path.PAGE_ERROR_PAGE;
        }
        return resp;
    }
}