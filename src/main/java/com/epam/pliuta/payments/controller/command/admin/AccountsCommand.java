package com.epam.pliuta.payments.controller.command.admin;

import com.epam.pliuta.payments.appcontext.AppContext;
import com.epam.pliuta.payments.controller.Path;
import com.epam.pliuta.payments.controller.command.ICommand;
import com.epam.pliuta.payments.model.entity.*;
import com.epam.pliuta.payments.model.service.IAccountService;
import com.epam.pliuta.payments.model.service.IUserService;
import com.epam.pliuta.payments.util.App;
import com.epam.pliuta.payments.util.PaginationSort;
import com.epam.pliuta.payments.util.RequestUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;

import static com.epam.pliuta.payments.controller.Path.PAGE_PARAMETER;
import static com.epam.pliuta.payments.controller.Path.SORT_PARAMETER;

/**
 * @author: Oleksandr Pliuta
 * @date: 04.06.2022
 */
public class AccountsCommand implements ICommand {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {

        IAccountService accountService = AppContext.getInstance().getAccountService();
        IUserService userService = AppContext.getInstance().getUserService();

        PaginationSort ps = new PaginationSort();
        ps.setSortType(RequestUtils.getStringParameter(request, SORT_PARAMETER));
        ps.setCurrentPage(RequestUtils.getIntParameter(request, PAGE_PARAMETER));
        ps.setNumberOfItems(accountService.findSize());
        ps.calc();

        List<Account> accounts = accountService.findAllLimitSort(ps.getStartFrom(),ps.MAX_ITEM_ON_PAGE, ps.getSortType());
        List<User> users = userService.findAll();
        HashMap<Long, User> usersHashMap = new HashMap<>();
        for (User user : users) {
            usersHashMap.put(user.getId(), user);
        }

        for (Account account : accounts) {
            String temp = usersHashMap.get(account.getUserID()).getFirstName() + " "
                    + usersHashMap.get(account.getUserID()).getLastName();
            account.setUserName(temp);
        }
        request.setAttribute("fullAccount", accounts);
        request.setAttribute("ps", ps);
        return Path.PAGE_ACCOUNTS;
    }
}

