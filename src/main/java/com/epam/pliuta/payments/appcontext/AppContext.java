package com.epam.pliuta.payments.appcontext;;

import com.epam.pliuta.payments.model.repository.IAccountRepo;
import com.epam.pliuta.payments.model.repository.ICardRepo;
import com.epam.pliuta.payments.model.repository.IPaymentRepo;
import com.epam.pliuta.payments.model.repository.IUserRepo;
import com.epam.pliuta.payments.model.repository.impl.AccountRepoImpl;
import com.epam.pliuta.payments.model.repository.impl.CardRepoImpl;
import com.epam.pliuta.payments.model.repository.impl.PaymentRepoImpl;
import com.epam.pliuta.payments.model.repository.impl.UserRepoImpl;
import com.epam.pliuta.payments.model.service.IAccountService;
import com.epam.pliuta.payments.model.service.ICardService;
import com.epam.pliuta.payments.model.service.IPaymentService;
import com.epam.pliuta.payments.model.service.IUserService;
import com.epam.pliuta.payments.model.service.impl.AccountServiceImpl;
import com.epam.pliuta.payments.model.service.impl.CardServiceImpl;
import com.epam.pliuta.payments.model.service.impl.PaymentServiceImpl;
import com.epam.pliuta.payments.model.service.impl.UserServiceImpl;

/**
 * @author: Oleksandr Pliuta
 * @date: 01.06.2022
 */
public class AppContext {
    private static volatile AppContext appContext = new AppContext();
    //repos

    private final IUserRepo userRepo = new UserRepoImpl();
    private final IAccountRepo accountRepo = new AccountRepoImpl();
    private final ICardRepo cardRepo = new CardRepoImpl();
    private final IPaymentRepo paymentRepo = new PaymentRepoImpl();


    private final ICardService cardService = new CardServiceImpl(cardRepo);
    private final IPaymentService paymentService = new PaymentServiceImpl(paymentRepo);
    private final IAccountService accountService = new AccountServiceImpl(accountRepo, cardService, paymentService);
    private final IUserService userService = new UserServiceImpl(userRepo,accountService);


    public static AppContext getInstance() {
        return appContext;
    }

    public ICardService getCardService() {
        return cardService;
    }

    public IUserService getUserService() {
        return userService;
    }

    public IPaymentService getPaymentService() {return paymentService;}

    public IAccountService getAccountService() {
        return accountService;
    }
}
