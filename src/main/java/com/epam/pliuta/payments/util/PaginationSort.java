package com.epam.pliuta.payments.util;

import com.epam.pliuta.payments.model.entity.Account;

import java.util.List;

/**
 * @author: Oleksandr Pliuta
 * @date: 05.06.2022
 */
public class PaginationSort {
    public final static int MAX_ITEM_ON_PAGE = 5;
    public final static String WITHOUT_SORT = "ws";
    public final static String NUMBER_STRAIGHT = "nus";
    public final static String NUMBER_FORWARD = "nuf";
    public final static String NAME_STRAIGHT = "nms";
    public final static String NAME_FORWARD = "nmf";
    public final static String BALANCE_STRAIGHT = "bs";
    public final static String BALANCE_FORWARD = "bf";
    public final static String DATE_STRAIGHT = "ds";
    public final static String DATE_FORWARD = "df";
    private int currentPage;
    private int numberOfPages;
    private long numberOfItems;
    private int prevPage;
    private int nextPage;
    private String sortType;
    private long startFrom;

    public long getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public long getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(int numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public long getNumberOfItems() {
        return numberOfItems;
    }

    public void setNumberOfItems(long numberOfItems) {
        this.numberOfItems = numberOfItems;
    }

    public String getSortType() {
        return sortType;
    }

    public void setSortType(String sortType) {
        this.sortType = sortType;
    }

    public int getPrevPage() {
        return prevPage;
    }

    public void setPrevPage(int prevPage) {
        this.prevPage = prevPage;
    }

    public int getNextPage() {
        return nextPage;
    }

    public void setNextPage(int nextPage) {
        this.nextPage = nextPage;
    }

    public long getStartFrom() {
        return startFrom;
    }

    public void setStartFrom(long startFrom) {
        this.startFrom = startFrom;
    }

    @Override
    public String toString() {
        return "PaginationSort{" +
                "currentPage=" + currentPage +
                ", numberOfPages=" + numberOfPages +
                ", numberOfItems=" + numberOfItems +
                ", prevPage=" + prevPage +
                ", nextPage=" + nextPage +
                ", sortType='" + sortType + '\'' +
                ", startFrom=" + startFrom +
                '}';
    }

    public void calc() {
        // Pagination
        this.numberOfPages = (int) this.numberOfItems / this.MAX_ITEM_ON_PAGE;
        if (this.numberOfItems % this.MAX_ITEM_ON_PAGE != 0) this.numberOfPages++;
        if (this.currentPage <= 1) {
            this.currentPage = 1;
        } else if (this.currentPage > this.numberOfPages) {
            this.currentPage = this.numberOfPages;
        }
        this.prevPage = this.currentPage - 1;
        if (this.prevPage == 0) this.prevPage = 1;
        this.nextPage = this.currentPage + 1;
        if (this.prevPage > this.numberOfPages) this.prevPage = this.numberOfPages;
        this.startFrom = (this.currentPage - 1) * this.MAX_ITEM_ON_PAGE;

        //Sorting
        if (this.sortType == null) {
            this.setSortType(WITHOUT_SORT);
        }
    }

}