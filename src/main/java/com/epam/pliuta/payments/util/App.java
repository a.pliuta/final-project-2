package com.epam.pliuta.payments.util;

import com.epam.pliuta.payments.model.entity.Payment;
import com.epam.pliuta.payments.model.entity.User;
import com.epam.pliuta.payments.model.repository.IAccountRepo;
import com.epam.pliuta.payments.model.repository.ICardRepo;
import com.epam.pliuta.payments.model.repository.IPaymentRepo;
import com.epam.pliuta.payments.model.repository.IUserRepo;
import com.epam.pliuta.payments.model.repository.impl.AccountRepoImpl;
import com.epam.pliuta.payments.model.repository.impl.CardRepoImpl;
import com.epam.pliuta.payments.model.repository.impl.PaymentRepoImpl;
import com.epam.pliuta.payments.model.repository.impl.UserRepoImpl;
import com.epam.pliuta.payments.model.service.IAccountService;
import com.epam.pliuta.payments.model.service.ICardService;
import com.epam.pliuta.payments.model.service.IPaymentService;
import com.epam.pliuta.payments.model.service.IUserService;
import com.epam.pliuta.payments.model.service.impl.AccountServiceImpl;
import com.epam.pliuta.payments.model.service.impl.CardServiceImpl;
import com.epam.pliuta.payments.model.service.impl.PaymentServiceImpl;
import com.epam.pliuta.payments.model.service.impl.UserServiceImpl;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * @author: Oleksandr Pliuta
 * @date: 31.05.2022
 */
public class App {
    public static <ICardSService> void main(String[] args) {

        final Logger LOG = Logger.getLogger(App.class);

        String login = "0679347143";
        final IUserRepo userRepo = new UserRepoImpl();
        final IAccountRepo accountRepo = new AccountRepoImpl();
        final ICardRepo cardRepo = new CardRepoImpl();
        final IPaymentRepo paymentRepo = new PaymentRepoImpl();

        final ICardService cardService = new CardServiceImpl(cardRepo);
        final IPaymentService paymentService = new PaymentServiceImpl(paymentRepo);

        final IAccountService accountService = new AccountServiceImpl(accountRepo, cardService, paymentService);
        final IUserService userService = new UserServiceImpl(userRepo,accountService);
        IUserService service = userService;
        IAccountService service2 = accountService;
        ICardService service3 =  cardService;
        IPaymentService service4 =  paymentService;

        long temp = service4.findSize();
        System.out.println(temp);

        List <Payment> payments= service4.findAllLimitSort(0,14, "nuf");
        System.out.println(payments);


        User user = service.findByIDFullInfo(2);

        System.out.println(user);




//
//        List <User> users= service.findAll();
//
//        for(User u: users)
//            System.out.println(u);
//
//        LOG.trace("I am trying to show users List");
//        List <User> users= service.findAllFullInfo();
//
//        for(User u: users)
//            System.out.println(u);
//
////        List<Account> accounts = service2.findByUserID(3);
////        for(Account a: accounts)
////            System.out.println(a);
//
//        List <Account> accounts= service2.findAllFullInfo();
//
//        for(Account a: accounts)
//            System.out.println(a);

//        List <Card> cards= service3.findAll();
//
//        for(Card a: cards)
//            System.out.println(a);

//        User user = service.findByLogin(login);
//        System.out.println(user);

//        User user= service.find(1);
//        user.setPassword(BCrypt.hashpw("12345678", BCrypt.gensalt()));
//        service.update(user);
//        User user= service.find(2);
//        user.setPassword(BCrypt.hashpw("11111111", BCrypt.gensalt()));
//        service.update(user);
//        user= service.find(3);
//        user.setPassword(BCrypt.hashpw("22222222", BCrypt.gensalt()));
//        service.update(user);
    }


}
