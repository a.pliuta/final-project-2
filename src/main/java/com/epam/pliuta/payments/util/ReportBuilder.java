package com.epam.pliuta.payments.util;

import com.epam.pliuta.payments.model.entity.Account;
import com.epam.pliuta.payments.model.entity.Card;
import com.epam.pliuta.payments.model.entity.Payment;
import com.epam.pliuta.payments.model.entity.User;
import com.epam.pliuta.payments.model.service.IUserService;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.time.format.DateTimeFormatter;
import java.util.Set;

/**
 * @author: Oleksandr Pliuta
 * @date: 26.06.2022
 */
public class ReportBuilder {
    public static void paymentPdf(HttpServletResponse response, Payment payment, User user, Card card){
        Document document = new Document(PageSize.A4, 50, 50, 50, 50);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        try {
            PdfWriter.getInstance(document, baos);
            document.open();

            BaseFont arial = BaseFont.createFont("C:\\Windows\\Fonts\\arial.ttf", "cp1251",
                    BaseFont.EMBEDDED);

            Paragraph title = new Paragraph("Платіж № " + payment.getNumber(), new Font(arial, 16));

            Chapter chapter = new Chapter(title, 1);
            chapter.setNumberDepth(0);

            chapter.add(new Paragraph("Користувач:              " + user.getFirstName() + " " + user.getLastName(), new Font(arial, 12)));
            chapter.add(new Paragraph("Сума платажа:         " + payment.getSum(), new Font(arial, 12)));
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
            chapter.add(new Paragraph("Час формування:     "  + payment.getFillDate().format(formatter), new Font(arial, 12)));
            if (payment.isSent()){
                chapter.add(new Paragraph("Час відправлення:   "   + payment.getSentDate().format(formatter), new Font(arial, 12)));
            } else{
                chapter.add(new Paragraph("Час відправлення:   -", new Font(arial, 12)));
            }
            chapter.add(new Paragraph("Номер картки:          " + card.getNumber(), new Font(arial, 12)));
            chapter.add(new Paragraph("\n", new Font(arial, 12)));

            chapter.add(new Paragraph("© 2022 Payments for everyone", new Font(arial, 6)));

            document.add(chapter);
            document.close();

            response.setHeader("Expires", "0");
            response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
            response.setHeader("Pragma", "public");
            // setting the content type
            response.setContentType("application/pdf");
            // the content length
            response.setContentLength(baos.size());
            // write ByteArrayOutputStream to the ServletOutputStream
            OutputStream os = null;
            try {
                os = response.getOutputStream();
                baos.writeTo(os);
                os.flush();
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (DocumentException | IOException e) {
            e.printStackTrace();
        }
    }
}
