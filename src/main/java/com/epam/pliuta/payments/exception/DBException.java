package com.epam.pliuta.payments.exception;

/**
 *  * An exception that provides information on a database access error.
 *
 * @author: Oleksandr Pliuta
 * @date: 03.06.2022
 */
public class DBException extends AppException {

    private static final long serialVersionUID = -3550446897536410392L;

    public DBException(String errCannotObtainDataById) {
        super();
    }

    public DBException(String message, Throwable cause) {
        super(message, cause);
    }

}

