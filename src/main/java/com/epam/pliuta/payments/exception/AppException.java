package com.epam.pliuta.payments.exception;

/**
 *  * An exception that provides information on an application error.
 *
 * @author: Oleksandr Pliuta
 * @date: 03.06.2022
 */
public class AppException extends Exception {

    private static final long serialVersionUID = 8288779062647218916L;

    public AppException() {
        super();
    }

    public AppException(String message, Throwable cause) {
        super(message, cause);
    }

    public AppException(String message) {
        super(message);
    }

}
