package com.epam.pliuta.payments.util;

import org.junit.jupiter.api.Test;

import static com.epam.pliuta.payments.util.Validator.*;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author: Oleksandr Pliuta
 * @date: 27.06.2022
 */
public class ValidatorTest {


    @Test
    void validateEMailSuccess() {
        assertEquals(null, validateEMail("123@gmail.com"));
    }
    @Test
    void validateEMailFailREGEX() {
        assertNotEquals(null, validateEMail("123.gmail.com"));
    }
    @Test
    void validateEMailFailNull() {
        assertNotEquals(null, validateEMail(null));
    }

    @Test
    void validatePhoneSuccess() {
        assertEquals(null, validatePhone("+380441112233"));
    }

    @Test
    void validateNamesFailREGEX() {
        assertNotEquals(null, validateEMail("+3804411122333"));
    }

    @Test
    void validateNamesFailNull() {
        assertNotEquals(null, validateEMail(null));
    }

    @Test
    void validateSentencesSuccess() {
        assertEquals(null, Validator.validateSentences("12345678901234567890", "password", 45));
    }

    @Test
    void validateSentencesFailLenght() {
        assertNotEquals(null, Validator.validateSentences("12345678901234567890", "password", 15));
    }

    @Test
    void validateLoginSuccess() {
        assertEquals(null, Validator.validateLogin("12345678901234567890",  20));
    }

    @Test
    void validateLoginFailData() {
        assertNotEquals(null, Validator.validateLogin("1234567890123456789A",  20));
    }

    @Test
    void validateCardNumberSuccess() {
        assertEquals(null, Validator.validateCardNumber("1234567890123456"));
    }

    @Test
    void validateCardNumberFailData() {
        assertNotEquals(null, Validator.validateCardNumber("ABCD567890123456"));
    }

    @Test
    void validateDateSuccess() {
        assertNotEquals(null,Validator.validateDate("2022-06-27 15:31:14"));
    }

    @Test
    void validateDateFailData() {
        assertNotEquals(null,Validator.validateDate("2022-06-127 15:31:14"));
    }

    @Test
    void validateAmmountSuccess() {
        assertEquals(null,Validator.validateAmmount("105.93"));
    }

    @Test
    void validateAmmountFailData() {
        assertNotEquals(null,Validator.validateAmmount("105,93"));
    }

    @Test
    void validateIntNumberSuccess() {
        assertEquals(null,Validator.validateIntNumber("10593"));
    }

    @Test
    void validateIntNumberFailData() {
        assertNotEquals(null,Validator.validateIntNumber("10A93"));
    }
}
