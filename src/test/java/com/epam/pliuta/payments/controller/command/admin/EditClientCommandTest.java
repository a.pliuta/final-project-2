package com.epam.pliuta.payments.controller.command.admin;

import com.epam.pliuta.payments.controller.Path;
import com.epam.pliuta.payments.model.entity.Account;
import com.epam.pliuta.payments.model.entity.User;
import com.epam.pliuta.payments.model.service.IAccountService;
import com.epam.pliuta.payments.model.service.IUserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mindrot.jbcrypt.BCrypt;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;

/**
 * @author: Oleksandr Pliuta
 * @date: 27.06.2022
 */
@ExtendWith(MockitoExtension.class)
public class EditClientCommandTest {
    @Mock
    HttpServletRequest req;
    @Mock
    HttpServletResponse resp;
    @Mock
    IUserService userService;
    @Mock
    ServletContext servletContext;

    @InjectMocks
    EditClientCommand cut;

    User user = new User();

    @BeforeEach
    public void setUp() {
        user.setLogin("user");
        user.setPassword(BCrypt.hashpw("pass", BCrypt.gensalt()));
        user.setRoleId(2);

    }

    @Test
    public void testBlockingUser() {
        Mockito.when(req.getParameter("user_id")).thenReturn("1");
        Mockito.when(userService.findByIDFullInfo(1)).thenReturn(user);
        Mockito.when(req.getServletContext()).thenReturn(servletContext);
        Mockito.when(req.getParameter("btnLock")).thenReturn("btnLock");
        String result = cut.execute(req, resp);
        Mockito.verify(userService, Mockito.times(1)).update(any(User.class));
        assertEquals(Path.COMMAND_REDIRECT, result);

    }
}
