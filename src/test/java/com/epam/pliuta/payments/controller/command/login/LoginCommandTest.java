package com.epam.pliuta.payments.controller.command.login;

import com.epam.pliuta.payments.controller.Path;
import com.epam.pliuta.payments.model.entity.User;
import com.epam.pliuta.payments.model.service.IUserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mindrot.jbcrypt.BCrypt;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author: Oleksandr Pliuta
 * @date: 27.06.2022
 */
@ExtendWith(MockitoExtension.class)
public class LoginCommandTest {
    @Mock
    private HttpServletRequest req;
    @Mock
    private HttpServletResponse resp;
    @Mock
    private IUserService userService;
    @Mock
    private HttpSession session;
    @InjectMocks
    private LoginCommand cut;

    private User user = new User();
    private User admin = new User();

    @BeforeEach
    public void setUp() {
        user.setLogin("user");
        user.setPassword(BCrypt.hashpw("pass", BCrypt.gensalt()));
        user.setActive(true);
        user.setRoleId(2);

        admin.setLogin("admin");
        admin.setPassword(BCrypt.hashpw("pass", BCrypt.gensalt()));
        admin.setActive(true);
        admin.setRoleId(1);
        //MockitoAnnotations.initMocks(this);
    }

    @Test
    public void ifUserIsUser() {
        Mockito.when(req.getParameter("login")).thenReturn("user");
        Mockito.when(req.getParameter("password")).thenReturn("pass");
        Mockito.when(userService.findByLogin("user")).thenReturn(user);
        Mockito.when(req.getSession()).thenReturn(session);

        String result = cut.execute(req, resp);
        assertEquals(Path.COMMAND_ACCOUNT, result);
    }

    @Test
    public void ifUserIsAdmin() {
        Mockito.when(req.getParameter("login")).thenReturn("admin");
        Mockito.when(req.getParameter("password")).thenReturn("pass");
        Mockito.when(userService.findByLogin("admin")).thenReturn(admin);
        Mockito.when(req.getSession()).thenReturn(session);

        String result = cut.execute(req, resp);
        assertEquals(Path.COMMAND_SHOW_USERS, result);
    }

    @Test
    public void ifUserIsBlank() {
        Mockito.when(req.getParameter("login")).thenReturn(null);
        Mockito.when(req.getParameter("password")).thenReturn(null);
        Mockito.when(req.getSession()).thenReturn(session);
        String result = cut.execute(req, resp);
        assertEquals(Path.PAGE_LOGIN, result);
    }

    @Test
    public void ifPasswordIsincorrect() {
        Mockito.when(req.getParameter("login")).thenReturn("admin");
        Mockito.when(req.getParameter("password")).thenReturn("pass__");
        Mockito.when(userService.findByLogin("admin")).thenReturn(admin);
        Mockito.when(req.getSession()).thenReturn(session);

        String result = cut.execute(req, resp);
        assertEquals(Path.PAGE_LOGIN, result);
    }
}
